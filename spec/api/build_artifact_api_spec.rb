=begin
#Bitrise API

#Official REST API for Bitrise.io

OpenAPI spec version: 0.1
Contact: letsconnect@bitrise.io
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.9

=end

require 'spec_helper'
require 'json'

# Unit tests for SwaggerClient::BuildArtifactApi
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'BuildArtifactApi' do
  before do
    # run before each test
    @instance = SwaggerClient::BuildArtifactApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of BuildArtifactApi' do
    it 'should create an instance of BuildArtifactApi' do
      expect(@instance).to be_instance_of(SwaggerClient::BuildArtifactApi)
    end
  end

  # unit tests for artifact_delete
  # Delete a build artifact
  # Delete a build artifact of an app&#39;s build. The required parameters are app slug, build slug and artifact slug. You can fetch the build artifact slug if you first list all build artifacts of an app with the [/apps/](https://api-docs.bitrise.io/#/build-artifact/artifact-list) endpoint.
  # @param app_slug App slug
  # @param build_slug Build slug
  # @param artifact_slug Artifact slug
  # @param [Hash] opts the optional parameters
  # @return [V0ArtifactDeleteResponseModel]
  describe 'artifact_delete test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for artifact_list
  # Get a list of all build artifacts
  # List all build artifacts that have been generated for an app&#39;s build. You can use the created build artifact slugs from the response output to retrieve data of a specific build artifact with the [GET/apps/](https://api-docs.bitrise.io/#/build-artifact/artifact-show) endpoint or update a build artifact with the [PATCH/apps](https://api-docs.bitrise.io/#/build-artifact/artifact-update) endpoint.
  # @param app_slug App slug
  # @param build_slug Build slug
  # @param [Hash] opts the optional parameters
  # @option opts [String] :_next Slug of the first build artifact in the response
  # @option opts [Integer] :limit Max number of build artifacts per page is 50.
  # @return [V0ArtifactListResponseModel]
  describe 'artifact_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for artifact_show
  # Get a specific build artifact
  # Retrieve data of a specific build artifact. The response output contains a time-limited download url (expires in 10 minutes) and a public install page URL. You can view the build artifact with both URLs, but the public install page url will not work unless you [enable it](https://devcenter.bitrise.io/tutorials/deploy/bitrise-app-deployment/#enabling-public-page-for-the-app).
  # @param app_slug App slug
  # @param build_slug Build slug
  # @param artifact_slug Artifact slug
  # @param [Hash] opts the optional parameters
  # @return [V0ArtifactShowResponseModel]
  describe 'artifact_show test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for artifact_update
  # Update a build artifact
  # Update the &#x60;is_public_page_enabled&#x60; attribute of your app&#39;s build. The required parameters are app slug, build slug and artifact slug. You can fetch the build artifact slug if you first list all build artifacts of an app with the [GET /apps/](https://api-docs.bitrise.io/#/build-artifact/artifact-list) endpoint.
  # @param app_slug App slug
  # @param build_slug Build slug
  # @param artifact_slug Artifact slug
  # @param artifact_params Artifact parameters
  # @param [Hash] opts the optional parameters
  # @return [V0ArtifactShowResponseModel]
  describe 'artifact_update test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
