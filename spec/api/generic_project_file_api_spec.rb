=begin
#Bitrise API

#Official REST API for Bitrise.io

OpenAPI spec version: 0.1
Contact: letsconnect@bitrise.io
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.9

=end

require 'spec_helper'
require 'json'

# Unit tests for SwaggerClient::GenericProjectFileApi
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'GenericProjectFileApi' do
  before do
    # run before each test
    @instance = SwaggerClient::GenericProjectFileApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of GenericProjectFileApi' do
    it 'should create an instance of GenericProjectFileApi' do
      expect(@instance).to be_instance_of(SwaggerClient::GenericProjectFileApi)
    end
  end

  # unit tests for generic_project_file_confirm
  # Confirm a generic project file upload
  # This is the last step of uploading a generic project file to Bitrise. Confirm the generic project file upload and view the file on the Code Signing tab of a specific app. Read more in our [Confirming the upload](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#confirming-the-file-upload) guide.
  # @param app_slug App slug
  # @param generic_project_file_slug Generic project file slug
  # @param [Hash] opts the optional parameters
  # @return [V0ProjectFileStorageResponseModel]
  describe 'generic_project_file_confirm test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for generic_project_file_delete
  # Delete a generic project file
  # Delete an app&#39;s generic project file. You can fetch an app&#39;s generic project file slug if you first list all the uploaded files with the [GET /apps/{app-slug}/generic-project-files](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-list) endpoint. Read more in our [Deleting a file](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#deleting-a-file) guide.
  # @param app_slug App slug
  # @param generic_project_file_slug Generic project file slug
  # @param [Hash] opts the optional parameters
  # @return [V0ProjectFileStorageResponseModel]
  describe 'generic_project_file_delete test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for generic_project_file_list
  # Get a list of the generic project files
  # List all the generic project files that have been uploaded to a specific app. Read more in our [Listing the uploaded files of an app](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#listing-the-uploaded-files-of-an-app) guide.
  # @param app_slug App slug
  # @param [Hash] opts the optional parameters
  # @option opts [String] :_next Slug of the first generic project file in the response
  # @option opts [Integer] :limit Max number of build certificates per page is 50.
  # @return [V0ProjectFileStorageListResponseModel]
  describe 'generic_project_file_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for generic_project_file_show
  # Get a specific generic project file
  # Retrieve data of a specific generic project file to check its attributes and optionally modify them with the [PATCH /apps/](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-update) endpoint. Read more in our [Retrieving a specific file&#39;s data](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#retrieving-a-specific-files-data) guide.
  # @param app_slug App slug
  # @param generic_project_file_slug Generic project file slug
  # @param [Hash] opts the optional parameters
  # @return [V0ProjectFileStorageResponseModel]
  describe 'generic_project_file_show test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for generic_project_file_update
  # Update a generic project file
  # Update a generic project file&#39;s attributes. You can fetch an app&#39;s generic project file slug if you first list all the uploaded files with the [GET /apps/{app-slug}/generic-project-files](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-list) endpoint. Read more in our [Updating an uploaded file](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#updating-an-uploaded-file) guide.
  # @param app_slug App slug
  # @param generic_project_file_slug Generic project file slug
  # @param generic_project_file Generic project file parameters
  # @param [Hash] opts the optional parameters
  # @return [V0ProjectFileStorageResponseModel]
  describe 'generic_project_file_update test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for generic_project_files_create
  # Create a generic project file
  # Create a temporary pre-signed upload URL (expires in 10 minutes) for the generic project file and upload it to AWS with a simple &#x60;curl&#x60; request. To complete the uploading process and view your files on the Code Signing tab of your app, continue with the [POST /apps/{app-slug}/generic-project-files/{generic-project-file-slug}/uploaded](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-confirm) endpoint. Read more in our [Creating and uploading files to Generic File Storage](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#creating-and-uploading-files-to-generic-file-storage) guide.
  # @param app_slug App slug
  # @param generic_project_file Generic project file parameters
  # @param [Hash] opts the optional parameters
  # @return [V0ProjectFileStorageResponseModel]
  describe 'generic_project_files_create test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
