=begin
#Bitrise API

#Official REST API for Bitrise.io

OpenAPI spec version: 0.1
Contact: letsconnect@bitrise.io
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.9

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for SwaggerClient::V0BuildRequestUpdateParams
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'V0BuildRequestUpdateParams' do
  before do
    # run before each test
    @instance = SwaggerClient::V0BuildRequestUpdateParams.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of V0BuildRequestUpdateParams' do
    it 'should create an instance of V0BuildRequestUpdateParams' do
      expect(@instance).to be_instance_of(SwaggerClient::V0BuildRequestUpdateParams)
    end
  end
  describe 'test attribute "is_approved"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
