=begin
#Bitrise API

#Official REST API for Bitrise.io

OpenAPI spec version: 0.1
Contact: letsconnect@bitrise.io
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.9

=end

require 'date'

module SwaggerClient
  class V0AppResponseItemModel
    attr_accessor :avatar_url

    attr_accessor :is_disabled

    attr_accessor :is_public

    attr_accessor :owner

    attr_accessor :project_type

    attr_accessor :provider

    attr_accessor :repo_owner

    attr_accessor :repo_slug

    attr_accessor :repo_url

    attr_accessor :slug

    attr_accessor :status

    attr_accessor :title

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'avatar_url' => :'avatar_url',
        :'is_disabled' => :'is_disabled',
        :'is_public' => :'is_public',
        :'owner' => :'owner',
        :'project_type' => :'project_type',
        :'provider' => :'provider',
        :'repo_owner' => :'repo_owner',
        :'repo_slug' => :'repo_slug',
        :'repo_url' => :'repo_url',
        :'slug' => :'slug',
        :'status' => :'status',
        :'title' => :'title'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'avatar_url' => :'String',
        :'is_disabled' => :'BOOLEAN',
        :'is_public' => :'BOOLEAN',
        :'owner' => :'V0OwnerAccountResponseModel',
        :'project_type' => :'String',
        :'provider' => :'String',
        :'repo_owner' => :'String',
        :'repo_slug' => :'String',
        :'repo_url' => :'String',
        :'slug' => :'String',
        :'status' => :'Integer',
        :'title' => :'String'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h| h[k.to_sym] = v }

      if attributes.has_key?(:'avatar_url')
        self.avatar_url = attributes[:'avatar_url']
      end

      if attributes.has_key?(:'is_disabled')
        self.is_disabled = attributes[:'is_disabled']
      end

      if attributes.has_key?(:'is_public')
        self.is_public = attributes[:'is_public']
      end

      if attributes.has_key?(:'owner')
        self.owner = attributes[:'owner']
      end

      if attributes.has_key?(:'project_type')
        self.project_type = attributes[:'project_type']
      end

      if attributes.has_key?(:'provider')
        self.provider = attributes[:'provider']
      end

      if attributes.has_key?(:'repo_owner')
        self.repo_owner = attributes[:'repo_owner']
      end

      if attributes.has_key?(:'repo_slug')
        self.repo_slug = attributes[:'repo_slug']
      end

      if attributes.has_key?(:'repo_url')
        self.repo_url = attributes[:'repo_url']
      end

      if attributes.has_key?(:'slug')
        self.slug = attributes[:'slug']
      end

      if attributes.has_key?(:'status')
        self.status = attributes[:'status']
      end

      if attributes.has_key?(:'title')
        self.title = attributes[:'title']
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          avatar_url == o.avatar_url &&
          is_disabled == o.is_disabled &&
          is_public == o.is_public &&
          owner == o.owner &&
          project_type == o.project_type &&
          provider == o.provider &&
          repo_owner == o.repo_owner &&
          repo_slug == o.repo_slug &&
          repo_url == o.repo_url &&
          slug == o.slug &&
          status == o.status &&
          title == o.title
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Fixnum] Hash code
    def hash
      [avatar_url, is_disabled, is_public, owner, project_type, provider, repo_owner, repo_slug, repo_url, slug, status, title].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.swagger_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :BOOLEAN
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        temp_model = SwaggerClient.const_get(type).new
        temp_model.build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        next if value.nil?
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end
  end
end
