=begin
#Bitrise API

#Official REST API for Bitrise.io

OpenAPI spec version: 0.1
Contact: letsconnect@bitrise.io
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.9

=end

require 'uri'

module SwaggerClient
  class BuildCertificateApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Confirm a build certificate upload
    # This is the last step of uploading a build certificate to Bitrise. Confirm the build certificate upload and view the file on the Code Signing tab of a specific app. Read more in our [Confirming the iOS code signing file upload](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param [Hash] opts the optional parameters
    # @return [V0BuildCertificateResponseModel]
    def build_certificate_confirm(app_slug, build_certificate_slug, opts = {})
      data, _status_code, _headers = build_certificate_confirm_with_http_info(app_slug, build_certificate_slug, opts)
      data
    end

    # Confirm a build certificate upload
    # This is the last step of uploading a build certificate to Bitrise. Confirm the build certificate upload and view the file on the Code Signing tab of a specific app. Read more in our [Confirming the iOS code signing file upload](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0BuildCertificateResponseModel, Fixnum, Hash)>] V0BuildCertificateResponseModel data, response status code and response headers
    def build_certificate_confirm_with_http_info(app_slug, build_certificate_slug, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BuildCertificateApi.build_certificate_confirm ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling BuildCertificateApi.build_certificate_confirm"
      end
      # verify the required parameter 'build_certificate_slug' is set
      if @api_client.config.client_side_validation && build_certificate_slug.nil?
        fail ArgumentError, "Missing the required parameter 'build_certificate_slug' when calling BuildCertificateApi.build_certificate_confirm"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/build-certificates/{build-certificate-slug}/uploaded'.sub('{' + 'app-slug' + '}', app_slug.to_s).sub('{' + 'build-certificate-slug' + '}', build_certificate_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0BuildCertificateResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BuildCertificateApi#build_certificate_confirm\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Create a build certificate
    # Create a temporary pre-signed upload URL for the build certificate and upload the file to AWS with a simple `curl` request. To complete the uploading process and view your files on the Code Signing tab of your app, continue with the [POST /apps/{app-slug}/build-certificates/{build-certificate-slug}/uploaded](https://api-docs.bitrise.io/#/build-certificate/build-certificate-confirm) endpoint. Read more in our [Creating and uploading an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#creating--uploading-an-ios-code-signing-file) guide.
    # @param app_slug App slug
    # @param build_certificate Build certificate parameters such as file name and its file size
    # @param [Hash] opts the optional parameters
    # @return [V0BuildCertificateResponseModel]
    def build_certificate_create(app_slug, build_certificate, opts = {})
      data, _status_code, _headers = build_certificate_create_with_http_info(app_slug, build_certificate, opts)
      data
    end

    # Create a build certificate
    # Create a temporary pre-signed upload URL for the build certificate and upload the file to AWS with a simple &#x60;curl&#x60; request. To complete the uploading process and view your files on the Code Signing tab of your app, continue with the [POST /apps/{app-slug}/build-certificates/{build-certificate-slug}/uploaded](https://api-docs.bitrise.io/#/build-certificate/build-certificate-confirm) endpoint. Read more in our [Creating and uploading an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#creating--uploading-an-ios-code-signing-file) guide.
    # @param app_slug App slug
    # @param build_certificate Build certificate parameters such as file name and its file size
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0BuildCertificateResponseModel, Fixnum, Hash)>] V0BuildCertificateResponseModel data, response status code and response headers
    def build_certificate_create_with_http_info(app_slug, build_certificate, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BuildCertificateApi.build_certificate_create ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling BuildCertificateApi.build_certificate_create"
      end
      # verify the required parameter 'build_certificate' is set
      if @api_client.config.client_side_validation && build_certificate.nil?
        fail ArgumentError, "Missing the required parameter 'build_certificate' when calling BuildCertificateApi.build_certificate_create"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/build-certificates'.sub('{' + 'app-slug' + '}', app_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(build_certificate)
      auth_names = ['PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0BuildCertificateResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BuildCertificateApi#build_certificate_create\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Delete a build certificate
    # Delete an app's build certificate. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint to list all available build certificates of an app. Read more in our [Deleting an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#deleting-an-ios-code-signing-file) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param [Hash] opts the optional parameters
    # @return [V0BuildCertificateResponseModel]
    def build_certificate_delete(app_slug, build_certificate_slug, opts = {})
      data, _status_code, _headers = build_certificate_delete_with_http_info(app_slug, build_certificate_slug, opts)
      data
    end

    # Delete a build certificate
    # Delete an app&#39;s build certificate. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint to list all available build certificates of an app. Read more in our [Deleting an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#deleting-an-ios-code-signing-file) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0BuildCertificateResponseModel, Fixnum, Hash)>] V0BuildCertificateResponseModel data, response status code and response headers
    def build_certificate_delete_with_http_info(app_slug, build_certificate_slug, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BuildCertificateApi.build_certificate_delete ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling BuildCertificateApi.build_certificate_delete"
      end
      # verify the required parameter 'build_certificate_slug' is set
      if @api_client.config.client_side_validation && build_certificate_slug.nil?
        fail ArgumentError, "Missing the required parameter 'build_certificate_slug' when calling BuildCertificateApi.build_certificate_delete"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/build-certificates/{build-certificate-slug}'.sub('{' + 'app-slug' + '}', app_slug.to_s).sub('{' + 'build-certificate-slug' + '}', build_certificate_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:DELETE, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0BuildCertificateResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BuildCertificateApi#build_certificate_delete\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Get a list of the build certificates
    # List all the build certificates that have been uploaded to a specific app. Read more in our [Listing the uploaded iOS code signing files of an app](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#listing-the-uploaded-ios-code-signing-files-of-an-app) guide.
    # @param app_slug App slug
    # @param [Hash] opts the optional parameters
    # @option opts [String] :_next Slug of the first build certificate in the response
    # @option opts [Integer] :limit Max number of build certificates per page is 50.
    # @return [V0BuildCertificateListResponseModel]
    def build_certificate_list(app_slug, opts = {})
      data, _status_code, _headers = build_certificate_list_with_http_info(app_slug, opts)
      data
    end

    # Get a list of the build certificates
    # List all the build certificates that have been uploaded to a specific app. Read more in our [Listing the uploaded iOS code signing files of an app](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#listing-the-uploaded-ios-code-signing-files-of-an-app) guide.
    # @param app_slug App slug
    # @param [Hash] opts the optional parameters
    # @option opts [String] :_next Slug of the first build certificate in the response
    # @option opts [Integer] :limit Max number of build certificates per page is 50.
    # @return [Array<(V0BuildCertificateListResponseModel, Fixnum, Hash)>] V0BuildCertificateListResponseModel data, response status code and response headers
    def build_certificate_list_with_http_info(app_slug, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BuildCertificateApi.build_certificate_list ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling BuildCertificateApi.build_certificate_list"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/build-certificates'.sub('{' + 'app-slug' + '}', app_slug.to_s)

      # query parameters
      query_params = {}
      query_params[:'next'] = opts[:'_next'] if !opts[:'_next'].nil?
      query_params[:'limit'] = opts[:'limit'] if !opts[:'limit'].nil?

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['AddonAuthToken', 'PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0BuildCertificateListResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BuildCertificateApi#build_certificate_list\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Get a specific build certificate
    # Retrieve data of a specific build certificate. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint to list all available build certificates of an app. Read more in our [Getting a specific iOS code signing file's data](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#getting-a-specific-ios-code-signing-files-data) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param [Hash] opts the optional parameters
    # @return [V0BuildCertificateResponseModel]
    def build_certificate_show(app_slug, build_certificate_slug, opts = {})
      data, _status_code, _headers = build_certificate_show_with_http_info(app_slug, build_certificate_slug, opts)
      data
    end

    # Get a specific build certificate
    # Retrieve data of a specific build certificate. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint to list all available build certificates of an app. Read more in our [Getting a specific iOS code signing file&#39;s data](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#getting-a-specific-ios-code-signing-files-data) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0BuildCertificateResponseModel, Fixnum, Hash)>] V0BuildCertificateResponseModel data, response status code and response headers
    def build_certificate_show_with_http_info(app_slug, build_certificate_slug, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BuildCertificateApi.build_certificate_show ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling BuildCertificateApi.build_certificate_show"
      end
      # verify the required parameter 'build_certificate_slug' is set
      if @api_client.config.client_side_validation && build_certificate_slug.nil?
        fail ArgumentError, "Missing the required parameter 'build_certificate_slug' when calling BuildCertificateApi.build_certificate_show"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/build-certificates/{build-certificate-slug}'.sub('{' + 'app-slug' + '}', app_slug.to_s).sub('{' + 'build-certificate-slug' + '}', build_certificate_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0BuildCertificateResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BuildCertificateApi#build_certificate_show\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Update a build certificate
    # Update an uploaded build certificate's attributes. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint. Read more in our [Updating an uploaded iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param build_certificate Build certificate parameters
    # @param [Hash] opts the optional parameters
    # @return [V0BuildCertificateResponseModel]
    def build_certificate_update(app_slug, build_certificate_slug, build_certificate, opts = {})
      data, _status_code, _headers = build_certificate_update_with_http_info(app_slug, build_certificate_slug, build_certificate, opts)
      data
    end

    # Update a build certificate
    # Update an uploaded build certificate&#39;s attributes. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint. Read more in our [Updating an uploaded iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.
    # @param app_slug App slug
    # @param build_certificate_slug Build certificate slug
    # @param build_certificate Build certificate parameters
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0BuildCertificateResponseModel, Fixnum, Hash)>] V0BuildCertificateResponseModel data, response status code and response headers
    def build_certificate_update_with_http_info(app_slug, build_certificate_slug, build_certificate, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BuildCertificateApi.build_certificate_update ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling BuildCertificateApi.build_certificate_update"
      end
      # verify the required parameter 'build_certificate_slug' is set
      if @api_client.config.client_side_validation && build_certificate_slug.nil?
        fail ArgumentError, "Missing the required parameter 'build_certificate_slug' when calling BuildCertificateApi.build_certificate_update"
      end
      # verify the required parameter 'build_certificate' is set
      if @api_client.config.client_side_validation && build_certificate.nil?
        fail ArgumentError, "Missing the required parameter 'build_certificate' when calling BuildCertificateApi.build_certificate_update"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/build-certificates/{build-certificate-slug}'.sub('{' + 'app-slug' + '}', app_slug.to_s).sub('{' + 'build-certificate-slug' + '}', build_certificate_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(build_certificate)
      auth_names = ['PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:PATCH, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0BuildCertificateResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BuildCertificateApi#build_certificate_update\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
