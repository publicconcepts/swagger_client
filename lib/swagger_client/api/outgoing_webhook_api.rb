=begin
#Bitrise API

#Official REST API for Bitrise.io

OpenAPI spec version: 0.1
Contact: letsconnect@bitrise.io
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.9

=end

require 'uri'

module SwaggerClient
  class OutgoingWebhookApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Create an outgoing webhook for an app
    # Create an outgoing webhook for a specified Bitrise app: this can be used to send build events to a specified URL with custom headers. Currently, only build events can trigger outgoing webhooks.
    # @param app_slug App slug
    # @param app_webhook_create_params App webhook creation params
    # @param [Hash] opts the optional parameters
    # @return [V0AppWebhookCreatedResponseModel]
    def outgoing_webhook_create(app_slug, app_webhook_create_params, opts = {})
      data, _status_code, _headers = outgoing_webhook_create_with_http_info(app_slug, app_webhook_create_params, opts)
      data
    end

    # Create an outgoing webhook for an app
    # Create an outgoing webhook for a specified Bitrise app: this can be used to send build events to a specified URL with custom headers. Currently, only build events can trigger outgoing webhooks.
    # @param app_slug App slug
    # @param app_webhook_create_params App webhook creation params
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0AppWebhookCreatedResponseModel, Fixnum, Hash)>] V0AppWebhookCreatedResponseModel data, response status code and response headers
    def outgoing_webhook_create_with_http_info(app_slug, app_webhook_create_params, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: OutgoingWebhookApi.outgoing_webhook_create ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling OutgoingWebhookApi.outgoing_webhook_create"
      end
      # verify the required parameter 'app_webhook_create_params' is set
      if @api_client.config.client_side_validation && app_webhook_create_params.nil?
        fail ArgumentError, "Missing the required parameter 'app_webhook_create_params' when calling OutgoingWebhookApi.outgoing_webhook_create"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/outgoing-webhooks'.sub('{' + 'app-slug' + '}', app_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(app_webhook_create_params)
      auth_names = ['AddonAuthToken', 'PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0AppWebhookCreatedResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: OutgoingWebhookApi#outgoing_webhook_create\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Delete an outgoing webhook of an app
    # Delete an existing outgoing webhook for a specified Bitrise app.
    # @param app_slug App slug
    # @param app_webhook_slug App webhook slug
    # @param [Hash] opts the optional parameters
    # @return [V0AppWebhookDeletedResponseModel]
    def outgoing_webhook_delete(app_slug, app_webhook_slug, opts = {})
      data, _status_code, _headers = outgoing_webhook_delete_with_http_info(app_slug, app_webhook_slug, opts)
      data
    end

    # Delete an outgoing webhook of an app
    # Delete an existing outgoing webhook for a specified Bitrise app.
    # @param app_slug App slug
    # @param app_webhook_slug App webhook slug
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0AppWebhookDeletedResponseModel, Fixnum, Hash)>] V0AppWebhookDeletedResponseModel data, response status code and response headers
    def outgoing_webhook_delete_with_http_info(app_slug, app_webhook_slug, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: OutgoingWebhookApi.outgoing_webhook_delete ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling OutgoingWebhookApi.outgoing_webhook_delete"
      end
      # verify the required parameter 'app_webhook_slug' is set
      if @api_client.config.client_side_validation && app_webhook_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_webhook_slug' when calling OutgoingWebhookApi.outgoing_webhook_delete"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/outgoing-webhooks/{app-webhook-slug}'.sub('{' + 'app-slug' + '}', app_slug.to_s).sub('{' + 'app-webhook-slug' + '}', app_webhook_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:DELETE, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0AppWebhookDeletedResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: OutgoingWebhookApi#outgoing_webhook_delete\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # List the outgoing webhooks of an app
    # List all the outgoing webhooks registered for a specified Bitrise app. This returns all the relevant data of the webhook, including the slug of the webhook and its URL.
    # @param app_slug App slug
    # @param [Hash] opts the optional parameters
    # @option opts [String] :_next Slug of the first webhook in the response
    # @option opts [Integer] :limit Max number of elements per page (default: 50)
    # @return [V0AppWebhookListResponseModel]
    def outgoing_webhook_list(app_slug, opts = {})
      data, _status_code, _headers = outgoing_webhook_list_with_http_info(app_slug, opts)
      data
    end

    # List the outgoing webhooks of an app
    # List all the outgoing webhooks registered for a specified Bitrise app. This returns all the relevant data of the webhook, including the slug of the webhook and its URL.
    # @param app_slug App slug
    # @param [Hash] opts the optional parameters
    # @option opts [String] :_next Slug of the first webhook in the response
    # @option opts [Integer] :limit Max number of elements per page (default: 50)
    # @return [Array<(V0AppWebhookListResponseModel, Fixnum, Hash)>] V0AppWebhookListResponseModel data, response status code and response headers
    def outgoing_webhook_list_with_http_info(app_slug, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: OutgoingWebhookApi.outgoing_webhook_list ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling OutgoingWebhookApi.outgoing_webhook_list"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/outgoing-webhooks'.sub('{' + 'app-slug' + '}', app_slug.to_s)

      # query parameters
      query_params = {}
      query_params[:'next'] = opts[:'_next'] if !opts[:'_next'].nil?
      query_params[:'limit'] = opts[:'limit'] if !opts[:'limit'].nil?

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['AddonAuthToken', 'PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0AppWebhookListResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: OutgoingWebhookApi#outgoing_webhook_list\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Update an outgoing webhook of an app
    # Update an existing outgoing webhook (URL, events, secrets and headers) for a specified Bitrise app. Even if you do not want to change one of the parameters, you still have to provide that parameter as well: simply use its existing value.
    # @param app_slug App slug
    # @param app_webhook_slug App webhook slug
    # @param app_webhook_update_params App webhook update params
    # @param [Hash] opts the optional parameters
    # @return [V0AppWebhookResponseModel]
    def outgoing_webhook_update(app_slug, app_webhook_slug, app_webhook_update_params, opts = {})
      data, _status_code, _headers = outgoing_webhook_update_with_http_info(app_slug, app_webhook_slug, app_webhook_update_params, opts)
      data
    end

    # Update an outgoing webhook of an app
    # Update an existing outgoing webhook (URL, events, secrets and headers) for a specified Bitrise app. Even if you do not want to change one of the parameters, you still have to provide that parameter as well: simply use its existing value.
    # @param app_slug App slug
    # @param app_webhook_slug App webhook slug
    # @param app_webhook_update_params App webhook update params
    # @param [Hash] opts the optional parameters
    # @return [Array<(V0AppWebhookResponseModel, Fixnum, Hash)>] V0AppWebhookResponseModel data, response status code and response headers
    def outgoing_webhook_update_with_http_info(app_slug, app_webhook_slug, app_webhook_update_params, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: OutgoingWebhookApi.outgoing_webhook_update ...'
      end
      # verify the required parameter 'app_slug' is set
      if @api_client.config.client_side_validation && app_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_slug' when calling OutgoingWebhookApi.outgoing_webhook_update"
      end
      # verify the required parameter 'app_webhook_slug' is set
      if @api_client.config.client_side_validation && app_webhook_slug.nil?
        fail ArgumentError, "Missing the required parameter 'app_webhook_slug' when calling OutgoingWebhookApi.outgoing_webhook_update"
      end
      # verify the required parameter 'app_webhook_update_params' is set
      if @api_client.config.client_side_validation && app_webhook_update_params.nil?
        fail ArgumentError, "Missing the required parameter 'app_webhook_update_params' when calling OutgoingWebhookApi.outgoing_webhook_update"
      end
      # resource path
      local_var_path = '/apps/{app-slug}/outgoing-webhooks/{app-webhook-slug}'.sub('{' + 'app-slug' + '}', app_slug.to_s).sub('{' + 'app-webhook-slug' + '}', app_webhook_slug.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(app_webhook_update_params)
      auth_names = ['PersonalAccessToken']
      data, status_code, headers = @api_client.call_api(:PUT, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'V0AppWebhookResponseModel')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: OutgoingWebhookApi#outgoing_webhook_update\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
