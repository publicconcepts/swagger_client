=begin
#Bitrise API

#Official REST API for Bitrise.io

OpenAPI spec version: 0.1
Contact: letsconnect@bitrise.io
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.9

=end

module SwaggerClient
  VERSION = '1.0.0'
end
