# SwaggerClient::BuildArtifactApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**artifact_delete**](BuildArtifactApi.md#artifact_delete) | **DELETE** /apps/{app-slug}/builds/{build-slug}/artifacts/{artifact-slug} | Delete a build artifact
[**artifact_list**](BuildArtifactApi.md#artifact_list) | **GET** /apps/{app-slug}/builds/{build-slug}/artifacts | Get a list of all build artifacts
[**artifact_show**](BuildArtifactApi.md#artifact_show) | **GET** /apps/{app-slug}/builds/{build-slug}/artifacts/{artifact-slug} | Get a specific build artifact
[**artifact_update**](BuildArtifactApi.md#artifact_update) | **PATCH** /apps/{app-slug}/builds/{build-slug}/artifacts/{artifact-slug} | Update a build artifact


# **artifact_delete**
> V0ArtifactDeleteResponseModel artifact_delete(app_slug, build_slug, artifact_slug)

Delete a build artifact

Delete a build artifact of an app's build. The required parameters are app slug, build slug and artifact slug. You can fetch the build artifact slug if you first list all build artifacts of an app with the [/apps/](https://api-docs.bitrise.io/#/build-artifact/artifact-list) endpoint.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildArtifactApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug

artifact_slug = 'artifact_slug_example' # String | Artifact slug


begin
  #Delete a build artifact
  result = api_instance.artifact_delete(app_slug, build_slug, artifact_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildArtifactApi->artifact_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 
 **artifact_slug** | **String**| Artifact slug | 

### Return type

[**V0ArtifactDeleteResponseModel**](V0ArtifactDeleteResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **artifact_list**
> V0ArtifactListResponseModel artifact_list(app_slug, build_slug, opts)

Get a list of all build artifacts

List all build artifacts that have been generated for an app's build. You can use the created build artifact slugs from the response output to retrieve data of a specific build artifact with the [GET/apps/](https://api-docs.bitrise.io/#/build-artifact/artifact-show) endpoint or update a build artifact with the [PATCH/apps](https://api-docs.bitrise.io/#/build-artifact/artifact-update) endpoint.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildArtifactApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug

opts = { 
  _next: '_next_example', # String | Slug of the first build artifact in the response
  limit: 56 # Integer | Max number of build artifacts per page is 50.
}

begin
  #Get a list of all build artifacts
  result = api_instance.artifact_list(app_slug, build_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildArtifactApi->artifact_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 
 **_next** | **String**| Slug of the first build artifact in the response | [optional] 
 **limit** | **Integer**| Max number of build artifacts per page is 50. | [optional] 

### Return type

[**V0ArtifactListResponseModel**](V0ArtifactListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **artifact_show**
> V0ArtifactShowResponseModel artifact_show(app_slug, build_slug, artifact_slug)

Get a specific build artifact

Retrieve data of a specific build artifact. The response output contains a time-limited download url (expires in 10 minutes) and a public install page URL. You can view the build artifact with both URLs, but the public install page url will not work unless you [enable it](https://devcenter.bitrise.io/tutorials/deploy/bitrise-app-deployment/#enabling-public-page-for-the-app).

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildArtifactApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug

artifact_slug = 'artifact_slug_example' # String | Artifact slug


begin
  #Get a specific build artifact
  result = api_instance.artifact_show(app_slug, build_slug, artifact_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildArtifactApi->artifact_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 
 **artifact_slug** | **String**| Artifact slug | 

### Return type

[**V0ArtifactShowResponseModel**](V0ArtifactShowResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **artifact_update**
> V0ArtifactShowResponseModel artifact_update(app_slug, build_slug, artifact_slug, artifact_params)

Update a build artifact

Update the `is_public_page_enabled` attribute of your app's build. The required parameters are app slug, build slug and artifact slug. You can fetch the build artifact slug if you first list all build artifacts of an app with the [GET /apps/](https://api-docs.bitrise.io/#/build-artifact/artifact-list) endpoint.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildArtifactApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug

artifact_slug = 'artifact_slug_example' # String | Artifact slug

artifact_params = SwaggerClient::V0ArtifactUpdateParams.new # V0ArtifactUpdateParams | Artifact parameters


begin
  #Update a build artifact
  result = api_instance.artifact_update(app_slug, build_slug, artifact_slug, artifact_params)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildArtifactApi->artifact_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 
 **artifact_slug** | **String**| Artifact slug | 
 **artifact_params** | [**V0ArtifactUpdateParams**](V0ArtifactUpdateParams.md)| Artifact parameters | 

### Return type

[**V0ArtifactShowResponseModel**](V0ArtifactShowResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



