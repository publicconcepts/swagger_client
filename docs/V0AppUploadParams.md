# SwaggerClient::V0AppUploadParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**git_owner** | **String** | The slug of the owner of the repository at the git provider | [optional] 
**git_repo_slug** | **String** | The slug of the repository at the git provider | [optional] 
**is_public** | **BOOLEAN** | If &#x60;true&#x60; then the repository visibility setting will be public, in case of &#x60;false&#x60; it will be private | [optional] 
**provider** | **String** | The git provider you are using, it can be &#x60;github&#x60;, &#x60;bitbucket&#x60;, &#x60;gitlab&#x60;, &#x60;gitlab-self-hosted&#x60; or &#x60;custom&#x60; | [optional] 
**repo_url** | **String** | The URL of your repository | [optional] 
**type** | **String** | It has to be provided by legacy reasons and has to have the &#x60;git&#x60; value | [optional] 


