# SwaggerClient::V0BuildListAllResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0BuildListAllResponseItemModel&gt;**](V0BuildListAllResponseItemModel.md) |  | [optional] 
**paging** | [**V0PagingResponseModel**](V0PagingResponseModel.md) | pagination | [optional] 


