# SwaggerClient::V0TestDeviceListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0TestDeviceResponseItemModel&gt;**](V0TestDeviceResponseItemModel.md) |  | [optional] 


