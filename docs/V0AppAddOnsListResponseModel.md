# SwaggerClient::V0AppAddOnsListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0AppAddOnResponseItemModel&gt;**](V0AppAddOnResponseItemModel.md) |  | [optional] 


