# SwaggerClient::V0ProjectFileStorageResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0ProjectFileStorageResponseItemModel**](V0ProjectFileStorageResponseItemModel.md) |  | [optional] 


