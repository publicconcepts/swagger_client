# SwaggerClient::V0AvatarPromoteResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_candidate** | **BOOLEAN** |  | [optional] 
**slug** | **String** |  | [optional] 
**upload_file_name** | **String** |  | [optional] 
**upload_file_size** | **Integer** |  | [optional] 


