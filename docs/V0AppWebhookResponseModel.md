# SwaggerClient::V0AppWebhookResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0AppWebhookResponseItemModel**](V0AppWebhookResponseItemModel.md) |  | [optional] 


