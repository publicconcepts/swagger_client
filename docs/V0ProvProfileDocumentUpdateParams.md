# SwaggerClient::V0ProvProfileDocumentUpdateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_expose** | **BOOLEAN** |  | [optional] 
**is_protected** | **BOOLEAN** |  | [optional] 
**processed** | **BOOLEAN** |  | [optional] 


