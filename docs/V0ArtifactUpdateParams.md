# SwaggerClient::V0ArtifactUpdateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_public_page_enabled** | **BOOLEAN** |  | [optional] 


