# SwaggerClient::V0AppWebhookResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **String** |  | [optional] 
**events** | **String** |  | [optional] 
**headers** | **String** |  | [optional] 
**registered_by_addon** | **BOOLEAN** |  | [optional] 
**slug** | **String** |  | [optional] 
**updated_at** | **String** |  | [optional] 
**url** | **String** |  | [optional] 


