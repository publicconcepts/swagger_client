# SwaggerClient::AddonsPlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**features** | [**Array&lt;AddonsFeature&gt;**](AddonsFeature.md) |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**price** | **Integer** |  | [optional] 


