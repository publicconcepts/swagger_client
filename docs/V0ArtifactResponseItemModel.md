# SwaggerClient::V0ArtifactResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**artifact_meta** | **String** |  | [optional] 
**artifact_type** | **String** |  | [optional] 
**expiring_download_url** | **String** |  | [optional] 
**file_size_bytes** | **Integer** |  | [optional] 
**is_public_page_enabled** | **BOOLEAN** |  | [optional] 
**public_install_page_url** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**title** | **String** |  | [optional] 


