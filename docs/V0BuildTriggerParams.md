# SwaggerClient::V0BuildTriggerParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**build_params** | [**V0BuildTriggerParamsBuildParams**](V0BuildTriggerParamsBuildParams.md) | The public part of the SSH key you would like to use | [optional] 
**hook_info** | [**V0BuildTriggerParamsHookInfo**](V0BuildTriggerParamsHookInfo.md) |  | [optional] 


