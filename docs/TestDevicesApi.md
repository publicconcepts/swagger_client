# SwaggerClient::TestDevicesApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**test_device_list**](TestDevicesApi.md#test_device_list) | **GET** /apps/{app-slug}/test-devices | List the test devices for an app


# **test_device_list**
> V0TestDeviceListResponseModel test_device_list(app_slug)

List the test devices for an app

List registered test devices of all members of a specified Bitrise app

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::TestDevicesApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #List the test devices for an app
  result = api_instance.test_device_list(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling TestDevicesApi->test_device_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0TestDeviceListResponseModel**](V0TestDeviceListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



