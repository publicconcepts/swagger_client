# SwaggerClient::V0BuildAbortParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**abort_reason** | **String** |  | [optional] 
**abort_with_success** | **BOOLEAN** |  | [optional] 
**skip_notifications** | **BOOLEAN** |  | [optional] 


