# SwaggerClient::V0AppRespModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slug** | **String** |  | [optional] 
**status** | **String** |  | [optional] 


