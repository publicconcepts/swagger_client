# SwaggerClient::V0ProvisionProfileResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**download_url** | **String** |  | [optional] 
**is_expose** | **BOOLEAN** |  | [optional] 
**is_protected** | **BOOLEAN** |  | [optional] 
**processed** | **BOOLEAN** |  | [optional] 
**slug** | **String** |  | [optional] 
**upload_file_name** | **String** |  | [optional] 
**upload_file_size** | **Integer** |  | [optional] 
**upload_url** | **String** |  | [optional] 


