# SwaggerClient::V0BuildShowResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0BuildResponseItemModel**](V0BuildResponseItemModel.md) |  | [optional] 


