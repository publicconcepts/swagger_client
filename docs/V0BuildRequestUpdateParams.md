# SwaggerClient::V0BuildRequestUpdateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_approved** | **BOOLEAN** |  | [optional] 


