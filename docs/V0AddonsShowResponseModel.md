# SwaggerClient::V0AddonsShowResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**AddonsAddon**](AddonsAddon.md) |  | [optional] 


