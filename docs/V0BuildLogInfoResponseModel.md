# SwaggerClient::V0BuildLogInfoResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiring_raw_log_url** | **String** |  | [optional] 
**generated_log_chunks_num** | **Integer** |  | [optional] 
**is_archived** | **BOOLEAN** |  | [optional] 
**log_chunks** | [**Array&lt;V0BuildLogChunkItemResponseModel&gt;**](V0BuildLogChunkItemResponseModel.md) |  | [optional] 
**timestamp** | **String** |  | [optional] 


