# SwaggerClient::V0OrganizationOwner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**username** | **String** |  | [optional] 


