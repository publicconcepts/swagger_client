# SwaggerClient::V0AppWebhookListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0AppWebhookResponseItemModel&gt;**](V0AppWebhookResponseItemModel.md) |  | [optional] 
**paging** | [**V0PagingResponseModel**](V0PagingResponseModel.md) | pagination | [optional] 


