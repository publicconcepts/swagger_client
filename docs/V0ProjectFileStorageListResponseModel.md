# SwaggerClient::V0ProjectFileStorageListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0ProjectFileStorageResponseItemModel&gt;**](V0ProjectFileStorageResponseItemModel.md) |  | [optional] 
**paging** | [**V0PagingResponseModel**](V0PagingResponseModel.md) | pagination | [optional] 


