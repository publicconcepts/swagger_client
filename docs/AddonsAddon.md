# SwaggerClient::AddonsAddon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**banner_image** | **String** |  | [optional] 
**card_header_colors** | **Array&lt;String&gt;** |  | [optional] 
**categories** | **Array&lt;String&gt;** |  | [optional] 
**description** | **String** |  | [optional] 
**developer_links** | [**Array&lt;AddonsDeveloperLink&gt;**](AddonsDeveloperLink.md) |  | [optional] 
**documentation_url** | **String** |  | [optional] 
**has_ui** | **BOOLEAN** |  | [optional] 
**icon** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**plans** | [**Array&lt;AddonsPlan&gt;**](AddonsPlan.md) |  | [optional] 
**platforms** | **Array&lt;String&gt;** |  | [optional] 
**preview_images** | **Array&lt;String&gt;** |  | [optional] 
**setup_guide** | **String** |  | [optional] 
**subtitle** | **String** |  | [optional] 
**summary** | **String** |  | [optional] 
**title** | **String** |  | [optional] 


