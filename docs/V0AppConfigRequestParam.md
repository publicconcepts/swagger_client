# SwaggerClient::V0AppConfigRequestParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_config_datastore_yaml** | **String** |  | [optional] 


