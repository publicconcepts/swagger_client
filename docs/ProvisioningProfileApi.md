# SwaggerClient::ProvisioningProfileApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**provisioning_profile_confirm**](ProvisioningProfileApi.md#provisioning_profile_confirm) | **POST** /apps/{app-slug}/provisioning-profiles/{provisioning-profile-slug}/uploaded | Confirm a provisioning profile upload
[**provisioning_profile_create**](ProvisioningProfileApi.md#provisioning_profile_create) | **POST** /apps/{app-slug}/provisioning-profiles | Create a provisioning profile
[**provisioning_profile_delete**](ProvisioningProfileApi.md#provisioning_profile_delete) | **DELETE** /apps/{app-slug}/provisioning-profiles/{provisioning-profile-slug} | Delete a provisioning profile
[**provisioning_profile_list**](ProvisioningProfileApi.md#provisioning_profile_list) | **GET** /apps/{app-slug}/provisioning-profiles | Get a list of the provisioning profiles
[**provisioning_profile_show**](ProvisioningProfileApi.md#provisioning_profile_show) | **GET** /apps/{app-slug}/provisioning-profiles/{provisioning-profile-slug} | Get a specific provisioning profile
[**provisioning_profile_update**](ProvisioningProfileApi.md#provisioning_profile_update) | **PATCH** /apps/{app-slug}/provisioning-profiles/{provisioning-profile-slug} | Update a provisioning profile


# **provisioning_profile_confirm**
> V0ProvisionProfileResponseModel provisioning_profile_confirm(app_slug, provisioning_profile_slug)

Confirm a provisioning profile upload

This is the last step of the upload process. Confirm the provisioning profile upload and view the file on the Code Signing tab of a specific app. Read more in our [Confirming the iOS code signing file upload](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProvisioningProfileApi.new

app_slug = 'app_slug_example' # String | App slug

provisioning_profile_slug = 'provisioning_profile_slug_example' # String | Provisioning profile slug


begin
  #Confirm a provisioning profile upload
  result = api_instance.provisioning_profile_confirm(app_slug, provisioning_profile_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProvisioningProfileApi->provisioning_profile_confirm: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **provisioning_profile_slug** | **String**| Provisioning profile slug | 

### Return type

[**V0ProvisionProfileResponseModel**](V0ProvisionProfileResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **provisioning_profile_create**
> V0ProvisionProfileResponseModel provisioning_profile_create(app_slug, provisioning_profile)

Create a provisioning profile

Create a temporary pre-signed upload URL (expires in 10 minutes) for the provisioning profile and upload it to AWS with a simple `curl` request. To complete the upload process, continue with the [POST /apps/{app-slug}/provisioning-profiles/{provisioning-profile-slug}/uploaded](https://api-docs.bitrise.io/#/provisioning-profile/provisioning-profile-confirm) endpoint. Read more in our [Creating and uploading an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#creating--uploading-an-ios-code-signing-file) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProvisioningProfileApi.new

app_slug = 'app_slug_example' # String | App slug

provisioning_profile = SwaggerClient::V0ProvisionProfileUploadParams.new # V0ProvisionProfileUploadParams | Provisioning profile parameters such as file name and file size


begin
  #Create a provisioning profile
  result = api_instance.provisioning_profile_create(app_slug, provisioning_profile)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProvisioningProfileApi->provisioning_profile_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **provisioning_profile** | [**V0ProvisionProfileUploadParams**](V0ProvisionProfileUploadParams.md)| Provisioning profile parameters such as file name and file size | 

### Return type

[**V0ProvisionProfileResponseModel**](V0ProvisionProfileResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **provisioning_profile_delete**
> V0ProvisionProfileResponseModel provisioning_profile_delete(app_slug, provisioning_profile_slug)

Delete a provisioning profile

Delete an app's provisioning profile. You can fetch the provisioning profile's slug if you call the [GET /apps/{app-slug}/provisioning-profiles](https://api-docs.bitrise.io/#/provisioning-profile/provisioning-profile-list) endpoint. Read more in our [Deleting an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#deleting-an-ios-code-signing-file) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProvisioningProfileApi.new

app_slug = 'app_slug_example' # String | App slug

provisioning_profile_slug = 'provisioning_profile_slug_example' # String | Provisioning profile slug


begin
  #Delete a provisioning profile
  result = api_instance.provisioning_profile_delete(app_slug, provisioning_profile_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProvisioningProfileApi->provisioning_profile_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **provisioning_profile_slug** | **String**| Provisioning profile slug | 

### Return type

[**V0ProvisionProfileResponseModel**](V0ProvisionProfileResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **provisioning_profile_list**
> V0ProvisionProfileListResponseModel provisioning_profile_list(app_slug, opts)

Get a list of the provisioning profiles

List all the provisioning profiles that have been uploaded to a specific app. Read more in our [Listing the uploaded iOS code signing files of an app](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#listing-the-uploaded-ios-code-signing-files-of-an-app) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProvisioningProfileApi.new

app_slug = 'app_slug_example' # String | App slug

opts = { 
  _next: '_next_example', # String | Slug of the first provisioning profile in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #Get a list of the provisioning profiles
  result = api_instance.provisioning_profile_list(app_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProvisioningProfileApi->provisioning_profile_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **_next** | **String**| Slug of the first provisioning profile in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0ProvisionProfileListResponseModel**](V0ProvisionProfileListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **provisioning_profile_show**
> V0ProvisionProfileResponseModel provisioning_profile_show(app_slug, provisioning_profile_slug)

Get a specific provisioning profile

Retrieve data of a specific provisioning profile. You can fetch the provisioning profile's slug if you call the [GET /apps/{app-slug}/provisioning-profiles](https://api-docs.bitrise.io/#/provisioning-profile/provisioning-profile-list) endpoint. Read more in our [Getting a specific iOS code signing file's data](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#getting-a-specific-ios-code-signing-files-data) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProvisioningProfileApi.new

app_slug = 'app_slug_example' # String | App slug

provisioning_profile_slug = 'provisioning_profile_slug_example' # String | Provisioning profile slug


begin
  #Get a specific provisioning profile
  result = api_instance.provisioning_profile_show(app_slug, provisioning_profile_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProvisioningProfileApi->provisioning_profile_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **provisioning_profile_slug** | **String**| Provisioning profile slug | 

### Return type

[**V0ProvisionProfileResponseModel**](V0ProvisionProfileResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **provisioning_profile_update**
> V0ProvisionProfileResponseModel provisioning_profile_update(app_slug, provisioning_profile_slug, provisioning_profile)

Update a provisioning profile

Update an uploaded provisioning profile's attributes. You can fetch the provisioning profile's slug if you call the [GET /apps/{app-slug}/provisioning-profiles](https://api-docs.bitrise.io/#/provisioning-profile/provisioning-profile-list) endpoint. Read more in our [Updating an uploaded iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProvisioningProfileApi.new

app_slug = 'app_slug_example' # String | App slug

provisioning_profile_slug = 'provisioning_profile_slug_example' # String | Provisioning profile slug

provisioning_profile = SwaggerClient::V0ProvProfileDocumentUpdateParams.new # V0ProvProfileDocumentUpdateParams | Provisioning profile parameters


begin
  #Update a provisioning profile
  result = api_instance.provisioning_profile_update(app_slug, provisioning_profile_slug, provisioning_profile)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProvisioningProfileApi->provisioning_profile_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **provisioning_profile_slug** | **String**| Provisioning profile slug | 
 **provisioning_profile** | [**V0ProvProfileDocumentUpdateParams**](V0ProvProfileDocumentUpdateParams.md)| Provisioning profile parameters | 

### Return type

[**V0ProvisionProfileResponseModel**](V0ProvisionProfileResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



