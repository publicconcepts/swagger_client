# SwaggerClient::V0BuildTriggerRespModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**build_number** | **Integer** |  | [optional] 
**build_slug** | **String** |  | [optional] 
**build_url** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**service** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**triggered_workflow** | **String** |  | [optional] 


