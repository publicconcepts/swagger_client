# SwaggerClient::V0BuildCertificateResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0BuildCertificateResponseItemModel**](V0BuildCertificateResponseItemModel.md) |  | [optional] 


