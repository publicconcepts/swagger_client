# SwaggerClient::V0AvatarPromoteResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0AvatarPromoteResponseItemModel**](V0AvatarPromoteResponseItemModel.md) |  | [optional] 


