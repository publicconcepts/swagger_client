# SwaggerClient::V0AppListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0AppResponseItemModel&gt;**](V0AppResponseItemModel.md) |  | [optional] 
**paging** | [**V0PagingResponseModel**](V0PagingResponseModel.md) | pagination | [optional] 


