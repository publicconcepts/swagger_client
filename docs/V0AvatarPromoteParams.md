# SwaggerClient::V0AvatarPromoteParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_promoted** | **BOOLEAN** |  | [optional] 


