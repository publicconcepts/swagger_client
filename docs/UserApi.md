# SwaggerClient::UserApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**user_plan**](UserApi.md#user_plan) | **GET** /me/plan | The subscription plan of the user
[**user_profile**](UserApi.md#user_profile) | **GET** /me | Get your profile data
[**user_show**](UserApi.md#user_show) | **GET** /users/{user-slug} | Get a specific user


# **user_plan**
> V0UserPlanRespModel user_plan

The subscription plan of the user

Get the subscription of the user: the current plan, any pending plans, and the duration of a trial period if applicable

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::UserApi.new

begin
  #The subscription plan of the user
  result = api_instance.user_plan
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling UserApi->user_plan: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**V0UserPlanRespModel**](V0UserPlanRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **user_profile**
> V0UserProfileRespModel user_profile

Get your profile data

Shows the authenticated users profile data

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::UserApi.new

begin
  #Get your profile data
  result = api_instance.user_profile
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling UserApi->user_profile: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**V0UserProfileRespModel**](V0UserProfileRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **user_show**
> V0UserProfileRespModel user_show(user_slug)

Get a specific user

Show information about a specific user

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::UserApi.new

user_slug = 'user_slug_example' # String | User slug


begin
  #Get a specific user
  result = api_instance.user_show(user_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling UserApi->user_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_slug** | **String**| User slug | 

### Return type

[**V0UserProfileRespModel**](V0UserProfileRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



