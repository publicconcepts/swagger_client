# SwaggerClient::V0UserProfileRespModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0UserProfileDataModel**](V0UserProfileDataModel.md) |  | [optional] 


