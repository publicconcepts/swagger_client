# SwaggerClient::V0BuildLogChunkItemResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chunk** | **String** |  | [optional] 
**position** | **Integer** |  | [optional] 


