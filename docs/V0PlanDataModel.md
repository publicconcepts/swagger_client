# SwaggerClient::V0PlanDataModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container_count** | **Integer** |  | [optional] 
**expires_at** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**price** | **Integer** |  | [optional] 


