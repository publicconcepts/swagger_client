# SwaggerClient::V0BuildTriggerParamsBuildParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branch** | **String** |  | [optional] 
**branch_dest** | **String** |  | [optional] 
**branch_dest_repo_owner** | **String** |  | [optional] 
**branch_repo_owner** | **String** |  | [optional] 
**build_request_slug** | **String** |  | [optional] 
**commit_hash** | **String** |  | [optional] 
**commit_message** | **String** |  | [optional] 
**commit_paths** | [**Array&lt;V0CommitPaths&gt;**](V0CommitPaths.md) |  | [optional] 
**diff_url** | **String** |  | [optional] 
**environments** | [**Array&lt;V0BuildParamsEnvironment&gt;**](V0BuildParamsEnvironment.md) |  | [optional] 
**pull_request_author** | **String** |  | [optional] 
**pull_request_head_branch** | **String** |  | [optional] 
**pull_request_id** | **Object** |  | [optional] 
**pull_request_merge_branch** | **String** |  | [optional] 
**pull_request_repository_url** | **String** |  | [optional] 
**skip_git_status_report** | **BOOLEAN** |  | [optional] 
**tag** | **String** |  | [optional] 
**workflow_id** | **String** |  | [optional] 


