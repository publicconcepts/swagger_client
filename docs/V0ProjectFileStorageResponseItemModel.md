# SwaggerClient::V0ProjectFileStorageResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**download_url** | **String** |  | [optional] 
**exposed_meta_datastore** | **String** |  | [optional] 
**is_expose** | **BOOLEAN** |  | [optional] 
**is_protected** | **BOOLEAN** |  | [optional] 
**processed** | **BOOLEAN** |  | [optional] 
**slug** | **String** |  | [optional] 
**upload_file_name** | **String** |  | [optional] 
**upload_file_size** | **Integer** |  | [optional] 
**upload_url** | **String** |  | [optional] 
**user_env_key** | **String** |  | [optional] 


