# SwaggerClient::BuildsApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**build_abort**](BuildsApi.md#build_abort) | **POST** /apps/{app-slug}/builds/{build-slug}/abort | Abort a specific build
[**build_bitrise_yml_show**](BuildsApi.md#build_bitrise_yml_show) | **GET** /apps/{app-slug}/builds/{build-slug}/bitrise.yml | Get the bitrise.yml of a build
[**build_list**](BuildsApi.md#build_list) | **GET** /apps/{app-slug}/builds | List all builds of an app
[**build_list_all**](BuildsApi.md#build_list_all) | **GET** /builds | List all builds
[**build_log**](BuildsApi.md#build_log) | **GET** /apps/{app-slug}/builds/{build-slug}/log | Get the build log of a build
[**build_show**](BuildsApi.md#build_show) | **GET** /apps/{app-slug}/builds/{build-slug} | Get a build of a given app
[**build_trigger**](BuildsApi.md#build_trigger) | **POST** /apps/{app-slug}/builds | Trigger a new build
[**build_workflow_list**](BuildsApi.md#build_workflow_list) | **GET** /apps/{app-slug}/build-workflows | List the workflows of an app


# **build_abort**
> V0BuildAbortResponseModel build_abort(app_slug, build_slug, build_abort_params)

Abort a specific build

Abort a specific build. Set an abort reason with the `abort_reason` parameter. Use the `abort_with_success` parameter to abort a build but still count it as a successful one.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug

build_abort_params = SwaggerClient::V0BuildAbortParams.new # V0BuildAbortParams | Build abort parameters


begin
  #Abort a specific build
  result = api_instance.build_abort(app_slug, build_slug, build_abort_params)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_abort: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 
 **build_abort_params** | [**V0BuildAbortParams**](V0BuildAbortParams.md)| Build abort parameters | 

### Return type

[**V0BuildAbortResponseModel**](V0BuildAbortResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **build_bitrise_yml_show**
> String build_bitrise_yml_show(app_slug, build_slug)

Get the bitrise.yml of a build

Get the bitrise.yml file of one of the builds of a given app. This will return the `bitrise.yml` configuration with which the build ran. You can compare it to [the current bitrise.yml configuration](https://api-docs.bitrise.io/#/application/app-config-datastore-show) of the app.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug


begin
  #Get the bitrise.yml of a build
  result = api_instance.build_bitrise_yml_show(app_slug, build_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_bitrise_yml_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 

### Return type

**String**

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain



# **build_list**
> V0BuildListResponseModel build_list(app_slug, opts)

List all builds of an app

List all the builds of a specified Bitrise app. Set parameters to filter builds: for example, you can search for builds run with a given workflow or all builds that were triggered by Pull Requests. It returns all the relevant data of the build.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

app_slug = 'app_slug_example' # String | App slug

opts = { 
  sort_by: 'sort_by_example', # String | Order of builds: sort them based on when they were created or the time when they were triggered
  branch: 'branch_example', # String | The branch which was built
  workflow: 'workflow_example', # String | The name of the workflow used for the build
  commit_message: 'commit_message_example', # String | The commit message of the build
  trigger_event_type: 'trigger_event_type_example', # String | The event that triggered the build (push, pull-request, tag)
  pull_request_id: 56, # Integer | The id of the pull request that triggered the build
  build_number: 56, # Integer | The build number
  after: 56, # Integer | List builds run after a given date (Unix Timestamp)
  before: 56, # Integer | List builds run before a given date (Unix Timestamp)
  status: 56, # Integer | The status of the build: not finished (0), successful (1), failed (2), aborted with failure (3), aborted with success (4)
  _next: '_next_example', # String | Slug of the first build in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #List all builds of an app
  result = api_instance.build_list(app_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **sort_by** | **String**| Order of builds: sort them based on when they were created or the time when they were triggered | [optional] 
 **branch** | **String**| The branch which was built | [optional] 
 **workflow** | **String**| The name of the workflow used for the build | [optional] 
 **commit_message** | **String**| The commit message of the build | [optional] 
 **trigger_event_type** | **String**| The event that triggered the build (push, pull-request, tag) | [optional] 
 **pull_request_id** | **Integer**| The id of the pull request that triggered the build | [optional] 
 **build_number** | **Integer**| The build number | [optional] 
 **after** | **Integer**| List builds run after a given date (Unix Timestamp) | [optional] 
 **before** | **Integer**| List builds run before a given date (Unix Timestamp) | [optional] 
 **status** | **Integer**| The status of the build: not finished (0), successful (1), failed (2), aborted with failure (3), aborted with success (4) | [optional] 
 **_next** | **String**| Slug of the first build in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0BuildListResponseModel**](V0BuildListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_list_all**
> V0BuildListAllResponseModel build_list_all(opts)

List all builds

List all the Bitrise builds that can be accessed with the authenticated account. Filter builds based on their owner, using the owner slug, or the status of the build.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

opts = { 
  owner_slug: 'owner_slug_example', # String | The slug of the owner of the app or apps
  is_on_hold: true, # BOOLEAN | Indicates whether the build has started yet (true: the build hasn't started)
  status: 56, # Integer | The status of the build: not finished (0), successful (1), failed (2), aborted with failure (3), aborted with success (4)
  _next: '_next_example', # String | Slug of the first build in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #List all builds
  result = api_instance.build_list_all(opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_list_all: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner_slug** | **String**| The slug of the owner of the app or apps | [optional] 
 **is_on_hold** | **BOOLEAN**| Indicates whether the build has started yet (true: the build hasn&#39;t started) | [optional] 
 **status** | **Integer**| The status of the build: not finished (0), successful (1), failed (2), aborted with failure (3), aborted with success (4) | [optional] 
 **_next** | **String**| Slug of the first build in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0BuildListAllResponseModel**](V0BuildListAllResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_log**
> V0BuildLogInfoResponseModel build_log(app_slug, build_slug)

Get the build log of a build

Get the build log of a specified build of a Bitrise app. You can get the build slug either by calling the [/builds](https://api-docs.bitrise.io/#/builds/build-list) endpoint or by clicking on the build on bitrise.io and copying the slug from the URL.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug


begin
  #Get the build log of a build
  result = api_instance.build_log(app_slug, build_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_log: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 

### Return type

[**V0BuildLogInfoResponseModel**](V0BuildLogInfoResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_show**
> V0BuildShowResponseModel build_show(app_slug, build_slug)

Get a build of a given app

Get the specified build of a given Bitrise app. You need to provide both an app slug and a build slug. You can get the build slug either by calling the [/builds](https://api-docs.bitrise.io/#/builds/build-list) endpoint or by clicking on the build on bitrise.io and copying the slug from the URL. The endpoint returns all the relevant data of the build.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

app_slug = 'app_slug_example' # String | App slug

build_slug = 'build_slug_example' # String | Build slug


begin
  #Get a build of a given app
  result = api_instance.build_show(app_slug, build_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_slug** | **String**| Build slug | 

### Return type

[**V0BuildShowResponseModel**](V0BuildShowResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_trigger**
> V0BuildTriggerRespModel build_trigger(app_slug, build_params)

Trigger a new build

Trigger a new build. Specify an app slug and at least one parameter out of three: a git tag or git commit hash, a branch, or a workflow ID. You can also set specific parameters for Pull Request builds and define additional environment variables for your build. [Check out our detailed guide](https://devcenter.bitrise.io/api/build-trigger/).

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

app_slug = 'app_slug_example' # String | App slug

build_params = SwaggerClient::V0BuildTriggerParams.new # V0BuildTriggerParams | Build trigger parameters


begin
  #Trigger a new build
  result = api_instance.build_trigger(app_slug, build_params)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_trigger: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_params** | [**V0BuildTriggerParams**](V0BuildTriggerParams.md)| Build trigger parameters | 

### Return type

[**V0BuildTriggerRespModel**](V0BuildTriggerRespModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **build_workflow_list**
> V0BuildWorkflowListResponseModel build_workflow_list(app_slug)

List the workflows of an app

List the workflows that were triggered at any time for a given Bitrise app. Note that it might list workflows that are currently not defined in the app's `bitrise.yml` configuration - and conversely, workflows that were never triggered will not be listed even if they are defined in the `bitrise.yml` file.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildsApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #List the workflows of an app
  result = api_instance.build_workflow_list(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildsApi->build_workflow_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0BuildWorkflowListResponseModel**](V0BuildWorkflowListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



