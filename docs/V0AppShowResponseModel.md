# SwaggerClient::V0AppShowResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0AppResponseItemModel**](V0AppResponseItemModel.md) |  | [optional] 


