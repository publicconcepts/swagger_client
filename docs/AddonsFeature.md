# SwaggerClient::AddonsFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available** | **BOOLEAN** |  | [optional] 
**description** | **String** |  | [optional] 
**quantity** | **String** |  | [optional] 


