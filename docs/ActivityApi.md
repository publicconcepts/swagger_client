# SwaggerClient::ActivityApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activity_list**](ActivityApi.md#activity_list) | **GET** /me/activities | Get list of Bitrise activity events


# **activity_list**
> V0ActivityEventListResponseModel activity_list(opts)

Get list of Bitrise activity events

List all the Bitrise activity events

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ActivityApi.new

opts = { 
  _next: '_next_example', # String | Slug of the first activity event in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #Get list of Bitrise activity events
  result = api_instance.activity_list(opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ActivityApi->activity_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_next** | **String**| Slug of the first activity event in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0ActivityEventListResponseModel**](V0ActivityEventListResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



