# SwaggerClient::V0ProjectFileStorageUploadParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**upload_file_name** | **String** |  | [optional] 
**upload_file_size** | **Integer** |  | [optional] 
**user_env_key** | **String** |  | [optional] 


