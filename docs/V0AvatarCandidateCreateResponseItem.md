# SwaggerClient::V0AvatarCandidateCreateResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** |  | [optional] 
**filesize** | **Integer** |  | [optional] 
**slug** | **String** |  | [optional] 
**upload_url** | **String** |  | [optional] 


