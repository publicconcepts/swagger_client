# SwaggerClient::V0FindAvatarCandidateResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**download_url** | **String** |  | [optional] 
**filename** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 


