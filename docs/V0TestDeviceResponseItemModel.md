# SwaggerClient::V0TestDeviceResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device_id** | **String** |  | [optional] 
**device_type** | **String** |  | [optional] 
**owner** | **String** |  | [optional] 


