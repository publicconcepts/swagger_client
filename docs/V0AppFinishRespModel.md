# SwaggerClient::V0AppFinishRespModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branch_name** | **String** |  | [optional] 
**build_trigger_token** | **String** |  | [optional] 
**is_webhook_auto_reg_supported** | **BOOLEAN** |  | [optional] 
**status** | **String** |  | [optional] 


