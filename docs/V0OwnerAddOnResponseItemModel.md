# SwaggerClient::V0OwnerAddOnResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apps** | [**Array&lt;V0AddOnAppResponseItemModel&gt;**](V0AddOnAppResponseItemModel.md) |  | [optional] 
**documentation_url** | **String** |  | [optional] 
**has_ui** | **BOOLEAN** |  | [optional] 
**icon** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**summary** | **String** |  | [optional] 
**title** | **String** |  | [optional] 


