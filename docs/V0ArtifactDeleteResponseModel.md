# SwaggerClient::V0ArtifactDeleteResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0ArtifactResponseItemModel**](V0ArtifactResponseItemModel.md) |  | [optional] 


