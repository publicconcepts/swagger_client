# SwaggerClient::V0AndroidKeystoreFileUploadParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_alias** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**private_key_password** | **String** |  | [optional] 
**upload_file_name** | **String** |  | [optional] 
**upload_file_size** | **Integer** |  | [optional] 


