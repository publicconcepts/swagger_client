# SwaggerClient::AndroidKeystoreFileApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**android_keystore_file_create**](AndroidKeystoreFileApi.md#android_keystore_file_create) | **POST** /apps/{app-slug}/android-keystore-files | Create an Android keystore file
[**android_keystore_file_list**](AndroidKeystoreFileApi.md#android_keystore_file_list) | **GET** /apps/{app-slug}/android-keystore-files | Get a list of the android keystore files


# **android_keystore_file_create**
> V0ProjectFileStorageResponseModel android_keystore_file_create(app_slug, android_keystore_file)

Create an Android keystore file

Add a new Android keystore file to an app

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AndroidKeystoreFileApi.new

app_slug = 'app_slug_example' # String | App slug

android_keystore_file = SwaggerClient::V0AndroidKeystoreFileUploadParams.new # V0AndroidKeystoreFileUploadParams | Android keystore file parameters


begin
  #Create an Android keystore file
  result = api_instance.android_keystore_file_create(app_slug, android_keystore_file)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AndroidKeystoreFileApi->android_keystore_file_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **android_keystore_file** | [**V0AndroidKeystoreFileUploadParams**](V0AndroidKeystoreFileUploadParams.md)| Android keystore file parameters | 

### Return type

[**V0ProjectFileStorageResponseModel**](V0ProjectFileStorageResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **android_keystore_file_list**
> V0ProjectFileStorageListResponseModel android_keystore_file_list(app_slug, opts)

Get a list of the android keystore files

List all the android keystore files that have been uploaded to a specific app.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AndroidKeystoreFileApi.new

app_slug = 'app_slug_example' # String | App slug

opts = { 
  _next: '_next_example', # String | Slug of the first android keystore file in the response
  limit: 56 # Integer | Max number of build certificates per page is 50.
}

begin
  #Get a list of the android keystore files
  result = api_instance.android_keystore_file_list(app_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AndroidKeystoreFileApi->android_keystore_file_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **_next** | **String**| Slug of the first android keystore file in the response | [optional] 
 **limit** | **Integer**| Max number of build certificates per page is 50. | [optional] 

### Return type

[**V0ProjectFileStorageListResponseModel**](V0ProjectFileStorageListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



