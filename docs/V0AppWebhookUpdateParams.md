# SwaggerClient::V0AppWebhookUpdateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | **String** |  | [optional] 
**headers** | **String** |  | [optional] 
**url** | **String** |  | [optional] 


