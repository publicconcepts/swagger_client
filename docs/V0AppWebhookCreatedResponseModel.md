# SwaggerClient::V0AppWebhookCreatedResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0AppWebhookResponseItemModel**](V0AppWebhookResponseItemModel.md) |  | [optional] 


