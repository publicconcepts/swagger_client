# SwaggerClient::V0OrganizationListRespModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0OrganizationDataModel&gt;**](V0OrganizationDataModel.md) |  | [optional] 


