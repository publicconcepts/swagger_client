# SwaggerClient::V0ArtifactShowResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0ArtifactResponseItemModel**](V0ArtifactResponseItemModel.md) |  | [optional] 


