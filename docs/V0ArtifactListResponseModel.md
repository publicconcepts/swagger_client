# SwaggerClient::V0ArtifactListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0ArtifactListElementResponseModel&gt;**](V0ArtifactListElementResponseModel.md) |  | [optional] 
**paging** | [**V0PagingResponseModel**](V0PagingResponseModel.md) | pagination | [optional] 


