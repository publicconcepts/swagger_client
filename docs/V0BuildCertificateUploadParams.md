# SwaggerClient::V0BuildCertificateUploadParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**upload_file_name** | **String** |  | [optional] 
**upload_file_size** | **Integer** |  | [optional] 


