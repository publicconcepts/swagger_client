# SwaggerClient::V0BuildRequestListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0BuildRequestResponseItemModel&gt;**](V0BuildRequestResponseItemModel.md) |  | [optional] 


