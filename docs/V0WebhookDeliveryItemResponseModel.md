# SwaggerClient::V0WebhookDeliveryItemResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **String** |  | [optional] 
**request_headers** | **String** |  | [optional] 
**request_payload** | **String** |  | [optional] 
**request_url** | **String** |  | [optional] 
**response_body** | **String** |  | [optional] 
**response_headers** | **String** |  | [optional] 
**response_http_status** | **String** |  | [optional] 
**response_seconds** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**updated_at** | **String** |  | [optional] 


