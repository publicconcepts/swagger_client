# SwaggerClient::AppSetupApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**app_config_create**](AppSetupApi.md#app_config_create) | **POST** /apps/{app-slug}/bitrise.yml | Upload a new bitrise.yml for your application.
[**app_create**](AppSetupApi.md#app_create) | **POST** /apps/register | Add a new app
[**app_finish**](AppSetupApi.md#app_finish) | **POST** /apps/{app-slug}/finish | Save the application at the end of the app registration process
[**app_webhook_create**](AppSetupApi.md#app_webhook_create) | **POST** /apps/{app-slug}/register-webhook | Register an incoming webhook for a specific application
[**ssh_key_create**](AppSetupApi.md#ssh_key_create) | **POST** /apps/{app-slug}/register-ssh-key | Add an SSH-key to a specific app


# **app_config_create**
> V0AppConfigRespModel app_config_create(app_slug, app_config)

Upload a new bitrise.yml for your application.

Upload a new bitrise.yml for your application.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AppSetupApi.new

app_slug = 'app_slug_example' # String | App slug

app_config = SwaggerClient::V0AppConfigRequestParam.new # V0AppConfigRequestParam | App config parameters


begin
  #Upload a new bitrise.yml for your application.
  result = api_instance.app_config_create(app_slug, app_config)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AppSetupApi->app_config_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app_config** | [**V0AppConfigRequestParam**](V0AppConfigRequestParam.md)| App config parameters | 

### Return type

[**V0AppConfigRespModel**](V0AppConfigRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_create**
> V0AppRespModel app_create(app)

Add a new app

Add a new app to Bitrise. This is the first step of the app registration process. To successfully set it up, you need to provide the required app parameters: your git provider, the repository URL, the slug of the repository as it appears at the provider, and the slug of the owner of the repository. Read more about the app creation process in our [detailed guide](https://devcenter.bitrise.io/api/adding-and-managing-apps/#adding-a-new-app).

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AppSetupApi.new

app = SwaggerClient::V0AppUploadParams.new # V0AppUploadParams | App parameters


begin
  #Add a new app
  result = api_instance.app_create(app)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AppSetupApi->app_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | [**V0AppUploadParams**](V0AppUploadParams.md)| App parameters | 

### Return type

[**V0AppRespModel**](V0AppRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_finish**
> V0AppFinishRespModel app_finish(app_slug, app)

Save the application at the end of the app registration process

Save the application after registering it on Bitrise and registering an SSH key (and, optionally, adding a webhook). With this endpoint you can define the initial configuration, define application-level environment variables, determine the project type, and set an Organization to be the owner of the app. Read more about the app registration process in our [detailed guide](https://devcenter.bitrise.io/api/adding-and-managing-apps/#adding-a-new-app).

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AppSetupApi.new

app_slug = 'app_slug_example' # String | App slug

app = SwaggerClient::V0AppFinishParams.new # V0AppFinishParams | App finish parameters


begin
  #Save the application at the end of the app registration process
  result = api_instance.app_finish(app_slug, app)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AppSetupApi->app_finish: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app** | [**V0AppFinishParams**](V0AppFinishParams.md)| App finish parameters | 

### Return type

[**V0AppFinishRespModel**](V0AppFinishRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_webhook_create**
> V0WebhookRespModel app_webhook_create(app_slug)

Register an incoming webhook for a specific application

[Register an incoming webhook](https://devcenter.bitrise.io/api/incoming-and-outgoing-webhooks/#incoming-webhooks) for a specific application. You can do this during the app registration process or at any other time in an app's life. When calling this endpoint, a webhook is registered at your git provider: this is necessary to automatically trigger builds on Bitrise.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AppSetupApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #Register an incoming webhook for a specific application
  result = api_instance.app_webhook_create(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AppSetupApi->app_webhook_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0WebhookRespModel**](V0WebhookRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **ssh_key_create**
> V0SSHKeyRespModel ssh_key_create(app_slug, ssh_key)

Add an SSH-key to a specific app

Add an SSH-key to a specific app. After creating an app, you need to register the SSH key so that Bitrise will be able to access and clone your repository during the build process. This requires the app slug of your newly created app.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AppSetupApi.new

app_slug = 'app_slug_example' # String | App slug

ssh_key = SwaggerClient::V0SSHKeyUploadParams.new # V0SSHKeyUploadParams | SSH key parameters


begin
  #Add an SSH-key to a specific app
  result = api_instance.ssh_key_create(app_slug, ssh_key)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AppSetupApi->ssh_key_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **ssh_key** | [**V0SSHKeyUploadParams**](V0SSHKeyUploadParams.md)| SSH key parameters | 

### Return type

[**V0SSHKeyRespModel**](V0SSHKeyRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



