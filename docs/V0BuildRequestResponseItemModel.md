# SwaggerClient::V0BuildRequestResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**build_params** | **String** |  | [optional] 
**created_at** | **String** |  | [optional] 
**pull_request_url** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 


