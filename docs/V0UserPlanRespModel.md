# SwaggerClient::V0UserPlanRespModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0UserPlanDataModel**](V0UserPlanDataModel.md) |  | [optional] 


