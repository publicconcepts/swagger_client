# SwaggerClient::V0ProjectFileStorageDocumentUpdateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exposed_meta_datastore** | **String** |  | [optional] 
**is_expose** | **BOOLEAN** |  | [optional] 
**is_protected** | **BOOLEAN** |  | [optional] 
**processed** | **BOOLEAN** |  | [optional] 
**user_env_key** | **String** |  | [optional] 


