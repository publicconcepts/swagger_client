# SwaggerClient::V0UserProfileDataModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatar_url** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**unconfirmed_email** | **String** |  | [optional] 
**username** | **String** |  | [optional] 


