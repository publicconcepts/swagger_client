# SwaggerClient::V0AppWebhookCreateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | **String** |  | [optional] 
**headers** | **String** |  | [optional] 
**secret** | **String** |  | [optional] 
**url** | **String** |  | [optional] 


