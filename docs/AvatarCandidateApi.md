# SwaggerClient::AvatarCandidateApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**avatar_candidate_create**](AvatarCandidateApi.md#avatar_candidate_create) | **POST** /apps/{app-slug}/avatar-candidates | Create avatar candidates
[**avatar_candidate_list**](AvatarCandidateApi.md#avatar_candidate_list) | **GET** /v0.1/apps/{app-slug}/avatar-candidates | Get list of the avatar candidates
[**avatar_candidate_promote**](AvatarCandidateApi.md#avatar_candidate_promote) | **PATCH** /apps/{app-slug}/avatar-candidates/{avatar-slug} | Promote an avatar candidate


# **avatar_candidate_create**
> V0AvatarCandidateCreateResponseItems avatar_candidate_create(app_slug, avatar_candidate)

Create avatar candidates

Add new avatar candidates to a specific app

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AvatarCandidateApi.new

app_slug = 'app_slug_example' # String | App slug

avatar_candidate = SwaggerClient::V0AvatarCandidateCreateBulkParams.new # V0AvatarCandidateCreateBulkParams | Avatar candidate parameters


begin
  #Create avatar candidates
  result = api_instance.avatar_candidate_create(app_slug, avatar_candidate)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AvatarCandidateApi->avatar_candidate_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **avatar_candidate** | [**V0AvatarCandidateCreateBulkParams**](V0AvatarCandidateCreateBulkParams.md)| Avatar candidate parameters | 

### Return type

[**V0AvatarCandidateCreateResponseItems**](V0AvatarCandidateCreateResponseItems.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **avatar_candidate_list**
> V0FindAvatarCandidateResponse avatar_candidate_list(app_slug)

Get list of the avatar candidates

List all available avatar candidates for an application

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AvatarCandidateApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #Get list of the avatar candidates
  result = api_instance.avatar_candidate_list(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AvatarCandidateApi->avatar_candidate_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0FindAvatarCandidateResponse**](V0FindAvatarCandidateResponse.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **avatar_candidate_promote**
> V0AvatarPromoteResponseModel avatar_candidate_promote(app_slug, avatar_slug, avatar_promote_params)

Promote an avatar candidate

Promotes an avatar candidate for an app

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AvatarCandidateApi.new

app_slug = 'app_slug_example' # String | App slug

avatar_slug = 'avatar_slug_example' # String | Avatar candidate slug

avatar_promote_params = SwaggerClient::V0AvatarPromoteParams.new # V0AvatarPromoteParams | Avatar promote parameters


begin
  #Promote an avatar candidate
  result = api_instance.avatar_candidate_promote(app_slug, avatar_slug, avatar_promote_params)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AvatarCandidateApi->avatar_candidate_promote: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **avatar_slug** | **String**| Avatar candidate slug | 
 **avatar_promote_params** | [**V0AvatarPromoteParams**](V0AvatarPromoteParams.md)| Avatar promote parameters | 

### Return type

[**V0AvatarPromoteResponseModel**](V0AvatarPromoteResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



