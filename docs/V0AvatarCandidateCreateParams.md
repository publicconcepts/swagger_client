# SwaggerClient::V0AvatarCandidateCreateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** |  | [optional] 
**filesize** | **Integer** |  | [optional] 


