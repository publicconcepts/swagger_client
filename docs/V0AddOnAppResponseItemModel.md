# SwaggerClient::V0AddOnAppResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icon** | **String** |  | [optional] 
**plan** | [**AddonsPlan**](AddonsPlan.md) |  | [optional] 
**plan_started_at** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**title** | **String** |  | [optional] 


