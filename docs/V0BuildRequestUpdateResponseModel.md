# SwaggerClient::V0BuildRequestUpdateResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0BuildRequestResponseItemModel**](V0BuildRequestResponseItemModel.md) |  | [optional] 


