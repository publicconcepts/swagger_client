# SwaggerClient::V0CommitPaths

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**added** | **Array&lt;String&gt;** |  | [optional] 
**modified** | **Array&lt;String&gt;** |  | [optional] 
**removed** | **Array&lt;String&gt;** |  | [optional] 


