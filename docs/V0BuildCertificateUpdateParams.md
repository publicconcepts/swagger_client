# SwaggerClient::V0BuildCertificateUpdateParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**certificate_password** | **String** |  | [optional] 
**is_expose** | **BOOLEAN** |  | [optional] 
**is_protected** | **BOOLEAN** |  | [optional] 
**processed** | **BOOLEAN** |  | [optional] 


