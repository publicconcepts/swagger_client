# SwaggerClient::V0ActivityEventResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**event_icon** | **String** |  | [optional] 
**event_stype** | **String** |  | [optional] 
**repository_avatar_icon_url** | **String** |  | [optional] 
**repository_title** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**target_path_string** | **String** |  | [optional] 
**title** | **String** |  | [optional] 


