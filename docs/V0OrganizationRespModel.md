# SwaggerClient::V0OrganizationRespModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0OrganizationDataModel**](V0OrganizationDataModel.md) |  | [optional] 


