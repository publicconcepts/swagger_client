# SwaggerClient::V0AvatarCandidateCreateBulkParamsInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** |  | [optional] 
**filesize** | **Integer** |  | [optional] 


