# SwaggerClient::OrganizationsApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**org_list**](OrganizationsApi.md#org_list) | **GET** /organizations | List the organizations that the user is part of
[**org_show**](OrganizationsApi.md#org_show) | **GET** /organizations/{org-slug} | Get a specified organization.


# **org_list**
> V0OrganizationListRespModel org_list

List the organizations that the user is part of

List all Bitrise organizations that the user is part of

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::OrganizationsApi.new

begin
  #List the organizations that the user is part of
  result = api_instance.org_list
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling OrganizationsApi->org_list: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**V0OrganizationListRespModel**](V0OrganizationListRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **org_show**
> V0OrganizationRespModel org_show(org_slug)

Get a specified organization.

Get a specified Bitrise organization that the user is part of.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::OrganizationsApi.new

org_slug = 'org_slug_example' # String | The organization slug


begin
  #Get a specified organization.
  result = api_instance.org_show(org_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling OrganizationsApi->org_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_slug** | **String**| The organization slug | 

### Return type

[**V0OrganizationRespModel**](V0OrganizationRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



