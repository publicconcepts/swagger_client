# SwaggerClient::WebhookDeliveryItemApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhook_delivery_item_list**](WebhookDeliveryItemApi.md#webhook_delivery_item_list) | **GET** /apps/{app-slug}/outgoing-webhooks/{app-webhook-slug}/delivery-items | List the webhook delivery items of an app
[**webhook_delivery_item_redeliver**](WebhookDeliveryItemApi.md#webhook_delivery_item_redeliver) | **POST** /apps/{app-slug}/outgoing-webhooks/{app-webhook-slug}/delivery-items/{webhook-delivery-item-slug}/redeliver | Re-deliver the webhook delivery items of an app
[**webhook_delivery_item_show**](WebhookDeliveryItemApi.md#webhook_delivery_item_show) | **GET** /apps/{app-slug}/outgoing-webhooks/{app-webhook-slug}/delivery-items/{webhook-delivery-item-slug} | Get a specific delivery item of a webhook


# **webhook_delivery_item_list**
> V0WebhookDeliveryItemShowResponseModel webhook_delivery_item_list(app_slug, app_webhook_slug, opts)

List the webhook delivery items of an app

List all the delivery items of an outgoing webhook of a Bitrise application

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::WebhookDeliveryItemApi.new

app_slug = 'app_slug_example' # String | App slug

app_webhook_slug = 'app_webhook_slug_example' # String | App webhook slug

opts = { 
  _next: '_next_example', # String | Slug of the first delivery item in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #List the webhook delivery items of an app
  result = api_instance.webhook_delivery_item_list(app_slug, app_webhook_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling WebhookDeliveryItemApi->webhook_delivery_item_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app_webhook_slug** | **String**| App webhook slug | 
 **_next** | **String**| Slug of the first delivery item in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0WebhookDeliveryItemShowResponseModel**](V0WebhookDeliveryItemShowResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **webhook_delivery_item_redeliver**
> ServiceStandardErrorRespModel webhook_delivery_item_redeliver(app_slug, app_webhook_slug, webhook_delivery_item_slug)

Re-deliver the webhook delivery items of an app

Re-deliver the delivery item of a specified webhook of a Bitrise application

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::WebhookDeliveryItemApi.new

app_slug = 'app_slug_example' # String | App slug

app_webhook_slug = 'app_webhook_slug_example' # String | App webhook slug

webhook_delivery_item_slug = 'webhook_delivery_item_slug_example' # String | Webhook delivery item slug


begin
  #Re-deliver the webhook delivery items of an app
  result = api_instance.webhook_delivery_item_redeliver(app_slug, app_webhook_slug, webhook_delivery_item_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling WebhookDeliveryItemApi->webhook_delivery_item_redeliver: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app_webhook_slug** | **String**| App webhook slug | 
 **webhook_delivery_item_slug** | **String**| Webhook delivery item slug | 

### Return type

[**ServiceStandardErrorRespModel**](ServiceStandardErrorRespModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **webhook_delivery_item_show**
> V0WebhookDeliveryItemResponseModel webhook_delivery_item_show(app_slug, app_webhook_slug, webhook_delivery_item_slug)

Get a specific delivery item of a webhook

Get the specified delivery item of an outgoing webhook of a Bitrise application

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::WebhookDeliveryItemApi.new

app_slug = 'app_slug_example' # String | App slug

app_webhook_slug = 'app_webhook_slug_example' # String | App webhook slug

webhook_delivery_item_slug = 'webhook_delivery_item_slug_example' # String | Webhook delivery item slug


begin
  #Get a specific delivery item of a webhook
  result = api_instance.webhook_delivery_item_show(app_slug, app_webhook_slug, webhook_delivery_item_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling WebhookDeliveryItemApi->webhook_delivery_item_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app_webhook_slug** | **String**| App webhook slug | 
 **webhook_delivery_item_slug** | **String**| Webhook delivery item slug | 

### Return type

[**V0WebhookDeliveryItemResponseModel**](V0WebhookDeliveryItemResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



