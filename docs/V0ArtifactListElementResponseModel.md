# SwaggerClient::V0ArtifactListElementResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**artifact_meta** | **String** |  | [optional] 
**artifact_type** | **String** |  | [optional] 
**file_size_bytes** | **Integer** |  | [optional] 
**is_public_page_enabled** | **BOOLEAN** |  | [optional] 
**slug** | **String** |  | [optional] 
**title** | **String** |  | [optional] 


