# SwaggerClient::V0OrganizationDataModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatar_icon_url** | **String** |  | [optional] 
**concurrency_count** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**owners** | [**Array&lt;V0OrganizationOwner&gt;**](V0OrganizationOwner.md) |  | [optional] 
**slug** | **String** |  | [optional] 


