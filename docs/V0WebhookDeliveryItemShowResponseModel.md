# SwaggerClient::V0WebhookDeliveryItemShowResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**V0WebhookDeliveryItemResponseModel**](V0WebhookDeliveryItemResponseModel.md) |  | [optional] 


