# SwaggerClient::V0BranchListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **Array&lt;String&gt;** |  | [optional] 


