# SwaggerClient::V0SSHKeyUploadParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth_ssh_private_key** | **String** | The private part of the SSH key you would like to use | [optional] 
**auth_ssh_public_key** | **String** | The public part of the SSH key you would like to use | [optional] 
**is_register_key_into_provider_service** | **BOOLEAN** | If it&#39;s set to true, the provided SSH key will be registered at the provider of the application | [optional] 


