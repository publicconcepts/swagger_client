# SwaggerClient::V0FindAvatarCandidateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0FindAvatarCandidateResponseItem&gt;**](V0FindAvatarCandidateResponseItem.md) |  | [optional] 


