# SwaggerClient::V0BuildWorkflowListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **Array&lt;String&gt;** |  | [optional] 


