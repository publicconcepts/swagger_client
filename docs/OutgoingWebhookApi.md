# SwaggerClient::OutgoingWebhookApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**outgoing_webhook_create**](OutgoingWebhookApi.md#outgoing_webhook_create) | **POST** /apps/{app-slug}/outgoing-webhooks | Create an outgoing webhook for an app
[**outgoing_webhook_delete**](OutgoingWebhookApi.md#outgoing_webhook_delete) | **DELETE** /apps/{app-slug}/outgoing-webhooks/{app-webhook-slug} | Delete an outgoing webhook of an app
[**outgoing_webhook_list**](OutgoingWebhookApi.md#outgoing_webhook_list) | **GET** /apps/{app-slug}/outgoing-webhooks | List the outgoing webhooks of an app
[**outgoing_webhook_update**](OutgoingWebhookApi.md#outgoing_webhook_update) | **PUT** /apps/{app-slug}/outgoing-webhooks/{app-webhook-slug} | Update an outgoing webhook of an app


# **outgoing_webhook_create**
> V0AppWebhookCreatedResponseModel outgoing_webhook_create(app_slug, app_webhook_create_params)

Create an outgoing webhook for an app

Create an outgoing webhook for a specified Bitrise app: this can be used to send build events to a specified URL with custom headers. Currently, only build events can trigger outgoing webhooks.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::OutgoingWebhookApi.new

app_slug = 'app_slug_example' # String | App slug

app_webhook_create_params = SwaggerClient::V0AppWebhookCreateParams.new # V0AppWebhookCreateParams | App webhook creation params


begin
  #Create an outgoing webhook for an app
  result = api_instance.outgoing_webhook_create(app_slug, app_webhook_create_params)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling OutgoingWebhookApi->outgoing_webhook_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app_webhook_create_params** | [**V0AppWebhookCreateParams**](V0AppWebhookCreateParams.md)| App webhook creation params | 

### Return type

[**V0AppWebhookCreatedResponseModel**](V0AppWebhookCreatedResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **outgoing_webhook_delete**
> V0AppWebhookDeletedResponseModel outgoing_webhook_delete(app_slug, app_webhook_slug)

Delete an outgoing webhook of an app

Delete an existing outgoing webhook for a specified Bitrise app.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::OutgoingWebhookApi.new

app_slug = 'app_slug_example' # String | App slug

app_webhook_slug = 'app_webhook_slug_example' # String | App webhook slug


begin
  #Delete an outgoing webhook of an app
  result = api_instance.outgoing_webhook_delete(app_slug, app_webhook_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling OutgoingWebhookApi->outgoing_webhook_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app_webhook_slug** | **String**| App webhook slug | 

### Return type

[**V0AppWebhookDeletedResponseModel**](V0AppWebhookDeletedResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **outgoing_webhook_list**
> V0AppWebhookListResponseModel outgoing_webhook_list(app_slug, opts)

List the outgoing webhooks of an app

List all the outgoing webhooks registered for a specified Bitrise app. This returns all the relevant data of the webhook, including the slug of the webhook and its URL.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::OutgoingWebhookApi.new

app_slug = 'app_slug_example' # String | App slug

opts = { 
  _next: '_next_example', # String | Slug of the first webhook in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #List the outgoing webhooks of an app
  result = api_instance.outgoing_webhook_list(app_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling OutgoingWebhookApi->outgoing_webhook_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **_next** | **String**| Slug of the first webhook in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0AppWebhookListResponseModel**](V0AppWebhookListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **outgoing_webhook_update**
> V0AppWebhookResponseModel outgoing_webhook_update(app_slug, app_webhook_slug, app_webhook_update_params)

Update an outgoing webhook of an app

Update an existing outgoing webhook (URL, events, secrets and headers) for a specified Bitrise app. Even if you do not want to change one of the parameters, you still have to provide that parameter as well: simply use its existing value.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::OutgoingWebhookApi.new

app_slug = 'app_slug_example' # String | App slug

app_webhook_slug = 'app_webhook_slug_example' # String | App webhook slug

app_webhook_update_params = SwaggerClient::V0AppWebhookUpdateParams.new # V0AppWebhookUpdateParams | App webhook update params


begin
  #Update an outgoing webhook of an app
  result = api_instance.outgoing_webhook_update(app_slug, app_webhook_slug, app_webhook_update_params)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling OutgoingWebhookApi->outgoing_webhook_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **app_webhook_slug** | **String**| App webhook slug | 
 **app_webhook_update_params** | [**V0AppWebhookUpdateParams**](V0AppWebhookUpdateParams.md)| App webhook update params | 

### Return type

[**V0AppWebhookResponseModel**](V0AppWebhookResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



