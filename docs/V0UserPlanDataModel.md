# SwaggerClient::V0UserPlanDataModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_plan** | [**V0PlanDataModel**](V0PlanDataModel.md) |  | [optional] 
**pending_plan** | [**V0PlanDataModel**](V0PlanDataModel.md) |  | [optional] 
**trial_expires_at** | **String** |  | [optional] 


