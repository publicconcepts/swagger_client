# SwaggerClient::V0ActivityEventListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0ActivityEventResponseItemModel&gt;**](V0ActivityEventResponseItemModel.md) |  | [optional] 
**paging** | [**V0PagingResponseModel**](V0PagingResponseModel.md) |  | [optional] 


