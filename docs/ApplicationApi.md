# SwaggerClient::ApplicationApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**app_config_datastore_show**](ApplicationApi.md#app_config_datastore_show) | **GET** /apps/{app-slug}/bitrise.yml | Get bitrise.yml of a specific app
[**app_list**](ApplicationApi.md#app_list) | **GET** /apps | Get list of the apps
[**app_list_by_organization**](ApplicationApi.md#app_list_by_organization) | **GET** /organizations/{org-slug}/apps | Get list of the apps for an organization
[**app_list_by_user**](ApplicationApi.md#app_list_by_user) | **GET** /users/{user-slug}/apps | Get list of the apps for a user
[**app_show**](ApplicationApi.md#app_show) | **GET** /apps/{app-slug} | Get a specific app
[**branch_list**](ApplicationApi.md#branch_list) | **GET** /apps/{app-slug}/branches | List the branches of an app&#39;s repository


# **app_config_datastore_show**
> String app_config_datastore_show(app_slug)

Get bitrise.yml of a specific app

Get the full `bitrise.yml` configuration of an application, by providing the app slug. It returns the current `bitrise.yml` that is stored on bitrise.io in full, including the trigger map, the different workflows and the Steps.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ApplicationApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #Get bitrise.yml of a specific app
  result = api_instance.app_config_datastore_show(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ApplicationApi->app_config_datastore_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

**String**

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain



# **app_list**
> V0AppListResponseModel app_list(opts)

Get list of the apps

List all the apps available for the authenticated account, including those that are owned by other users or Organizations.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ApplicationApi.new

opts = { 
  sort_by: 'sort_by_example', # String | Order of the applications: sort them based on when they were created or the time of their last build
  _next: '_next_example', # String | Slug of the first app in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #Get list of the apps
  result = api_instance.app_list(opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ApplicationApi->app_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sort_by** | **String**| Order of the applications: sort them based on when they were created or the time of their last build | [optional] 
 **_next** | **String**| Slug of the first app in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0AppListResponseModel**](V0AppListResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_list_by_organization**
> V0AppListResponseModel app_list_by_organization(org_slug, opts)

Get list of the apps for an organization

List all the available apps owned by a given organization. [Find the organization URL](https://devcenter.bitrise.io/team-management/organizations/org-url/) of the organisations you are part of; be aware that the endpoint will not return any apps if the authenticated account is not a member of the given organisation.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ApplicationApi.new

org_slug = 'org_slug_example' # String | Organization slug

opts = { 
  sort_by: 'sort_by_example', # String | Order of applications: sort them based on when they were created or the time of their last build
  _next: '_next_example', # String | Slug of the first app in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #Get list of the apps for an organization
  result = api_instance.app_list_by_organization(org_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ApplicationApi->app_list_by_organization: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_slug** | **String**| Organization slug | 
 **sort_by** | **String**| Order of applications: sort them based on when they were created or the time of their last build | [optional] 
 **_next** | **String**| Slug of the first app in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0AppListResponseModel**](V0AppListResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_list_by_user**
> V0AppListResponseModel app_list_by_user(user_slug, opts)

Get list of the apps for a user

List all the available apps for the given user.  It needs the user slug that you can get from the [GET /me](https://api-docs.bitrise.io/#/user/user-profile) endpoint.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ApplicationApi.new

user_slug = 'user_slug_example' # String | User slug

opts = { 
  sort_by: 'sort_by_example', # String | Order of applications
  _next: '_next_example', # String | Slug of the first app in the response
  limit: 56 # Integer | Max number of elements per page (default: 50)
}

begin
  #Get list of the apps for a user
  result = api_instance.app_list_by_user(user_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ApplicationApi->app_list_by_user: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_slug** | **String**| User slug | 
 **sort_by** | **String**| Order of applications | [optional] 
 **_next** | **String**| Slug of the first app in the response | [optional] 
 **limit** | **Integer**| Max number of elements per page (default: 50) | [optional] 

### Return type

[**V0AppListResponseModel**](V0AppListResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_show**
> V0AppShowResponseModel app_show(app_slug)

Get a specific app

Get the details of a specific app by providing the app slug. You can get the app slug by calling the [/apps](https://api-docs.bitrise.io/#/application/app-list) endpoint or by opening the app on bitrise.io and copying the slug from the URL.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ApplicationApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #Get a specific app
  result = api_instance.app_show(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ApplicationApi->app_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0AppShowResponseModel**](V0AppShowResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **branch_list**
> V0BranchListResponseModel branch_list(app_slug)

List the branches of an app's repository

List the existing branches of the repository of a specified Bitrise app.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ApplicationApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #List the branches of an app's repository
  result = api_instance.branch_list(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ApplicationApi->branch_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0BranchListResponseModel**](V0BranchListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



