# SwaggerClient::V0BuildParamsEnvironment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_expand** | **BOOLEAN** |  | [optional] 
**mapped_to** | **String** |  | [optional] 
**value** | **String** |  | [optional] 


