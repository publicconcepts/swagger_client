# SwaggerClient::V0AppResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatar_url** | **String** |  | [optional] 
**is_disabled** | **BOOLEAN** |  | [optional] 
**is_public** | **BOOLEAN** |  | [optional] 
**owner** | [**V0OwnerAccountResponseModel**](V0OwnerAccountResponseModel.md) |  | [optional] 
**project_type** | **String** |  | [optional] 
**provider** | **String** |  | [optional] 
**repo_owner** | **String** |  | [optional] 
**repo_slug** | **String** |  | [optional] 
**repo_url** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**status** | **Integer** |  | [optional] 
**title** | **String** |  | [optional] 


