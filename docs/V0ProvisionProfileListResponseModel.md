# SwaggerClient::V0ProvisionProfileListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;V0ProvisionProfileResponseItemModel&gt;**](V0ProvisionProfileResponseItemModel.md) |  | [optional] 
**paging** | [**V0PagingResponseModel**](V0PagingResponseModel.md) | pagination | [optional] 


