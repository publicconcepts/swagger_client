# SwaggerClient::V0AppAddOnResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentation_url** | **String** |  | [optional] 
**has_ui** | **BOOLEAN** |  | [optional] 
**icon** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**plan** | [**AddonsPlan**](AddonsPlan.md) |  | [optional] 
**summary** | **String** |  | [optional] 
**title** | **String** |  | [optional] 


