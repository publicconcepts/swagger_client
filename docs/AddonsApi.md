# SwaggerClient::AddonsApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addon_list_by_app**](AddonsApi.md#addon_list_by_app) | **GET** /apps/{app-slug}/addons | Get list of the addons for apps
[**addon_list_by_organization**](AddonsApi.md#addon_list_by_organization) | **GET** /organizations/{organization-slug}/addons | Get list of the addons for organization
[**addon_list_by_user**](AddonsApi.md#addon_list_by_user) | **GET** /users/{user-slug}/addons | Get list of the addons for user
[**addons_list**](AddonsApi.md#addons_list) | **GET** /addons | Get list of available Bitrise addons
[**addons_show**](AddonsApi.md#addons_show) | **GET** /addons/{addon-id} | Get a specific Bitrise addon


# **addon_list_by_app**
> V0AppAddOnsListResponseModel addon_list_by_app(app_slug)

Get list of the addons for apps

List all the provisioned addons for the authorized apps

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AddonsApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #Get list of the addons for apps
  result = api_instance.addon_list_by_app(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AddonsApi->addon_list_by_app: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0AppAddOnsListResponseModel**](V0AppAddOnsListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **addon_list_by_organization**
> V0OwnerAddOnsListResponseModel addon_list_by_organization(organization_slug)

Get list of the addons for organization

List all the provisioned addons for organization

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AddonsApi.new

organization_slug = 'organization_slug_example' # String | Organization slug


begin
  #Get list of the addons for organization
  result = api_instance.addon_list_by_organization(organization_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AddonsApi->addon_list_by_organization: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_slug** | **String**| Organization slug | 

### Return type

[**V0OwnerAddOnsListResponseModel**](V0OwnerAddOnsListResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **addon_list_by_user**
> V0OwnerAddOnsListResponseModel addon_list_by_user(user_slug)

Get list of the addons for user

List all the provisioned addons for the authenticated user

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AddonsApi.new

user_slug = 'user_slug_example' # String | User slug


begin
  #Get list of the addons for user
  result = api_instance.addon_list_by_user(user_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AddonsApi->addon_list_by_user: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_slug** | **String**| User slug | 

### Return type

[**V0OwnerAddOnsListResponseModel**](V0OwnerAddOnsListResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **addons_list**
> V0AddonsListResponseModel addons_list

Get list of available Bitrise addons

List all the available Bitrise addons

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AddonsApi.new

begin
  #Get list of available Bitrise addons
  result = api_instance.addons_list
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AddonsApi->addons_list: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**V0AddonsListResponseModel**](V0AddonsListResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **addons_show**
> V0AddonsShowResponseModel addons_show(addon_id)

Get a specific Bitrise addon

Show details of a specific Bitrise addon

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::AddonsApi.new

addon_id = 'addon_id_example' # String | Addon ID


begin
  #Get a specific Bitrise addon
  result = api_instance.addons_show(addon_id)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling AddonsApi->addons_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addon_id** | **String**| Addon ID | 

### Return type

[**V0AddonsShowResponseModel**](V0AddonsShowResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



