# SwaggerClient::BuildCertificateApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**build_certificate_confirm**](BuildCertificateApi.md#build_certificate_confirm) | **POST** /apps/{app-slug}/build-certificates/{build-certificate-slug}/uploaded | Confirm a build certificate upload
[**build_certificate_create**](BuildCertificateApi.md#build_certificate_create) | **POST** /apps/{app-slug}/build-certificates | Create a build certificate
[**build_certificate_delete**](BuildCertificateApi.md#build_certificate_delete) | **DELETE** /apps/{app-slug}/build-certificates/{build-certificate-slug} | Delete a build certificate
[**build_certificate_list**](BuildCertificateApi.md#build_certificate_list) | **GET** /apps/{app-slug}/build-certificates | Get a list of the build certificates
[**build_certificate_show**](BuildCertificateApi.md#build_certificate_show) | **GET** /apps/{app-slug}/build-certificates/{build-certificate-slug} | Get a specific build certificate
[**build_certificate_update**](BuildCertificateApi.md#build_certificate_update) | **PATCH** /apps/{app-slug}/build-certificates/{build-certificate-slug} | Update a build certificate


# **build_certificate_confirm**
> V0BuildCertificateResponseModel build_certificate_confirm(app_slug, build_certificate_slug)

Confirm a build certificate upload

This is the last step of uploading a build certificate to Bitrise. Confirm the build certificate upload and view the file on the Code Signing tab of a specific app. Read more in our [Confirming the iOS code signing file upload](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildCertificateApi.new

app_slug = 'app_slug_example' # String | App slug

build_certificate_slug = 'build_certificate_slug_example' # String | Build certificate slug


begin
  #Confirm a build certificate upload
  result = api_instance.build_certificate_confirm(app_slug, build_certificate_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildCertificateApi->build_certificate_confirm: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_certificate_slug** | **String**| Build certificate slug | 

### Return type

[**V0BuildCertificateResponseModel**](V0BuildCertificateResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **build_certificate_create**
> V0BuildCertificateResponseModel build_certificate_create(app_slug, build_certificate)

Create a build certificate

Create a temporary pre-signed upload URL for the build certificate and upload the file to AWS with a simple `curl` request. To complete the uploading process and view your files on the Code Signing tab of your app, continue with the [POST /apps/{app-slug}/build-certificates/{build-certificate-slug}/uploaded](https://api-docs.bitrise.io/#/build-certificate/build-certificate-confirm) endpoint. Read more in our [Creating and uploading an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#creating--uploading-an-ios-code-signing-file) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildCertificateApi.new

app_slug = 'app_slug_example' # String | App slug

build_certificate = SwaggerClient::V0BuildCertificateUploadParams.new # V0BuildCertificateUploadParams | Build certificate parameters such as file name and its file size


begin
  #Create a build certificate
  result = api_instance.build_certificate_create(app_slug, build_certificate)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildCertificateApi->build_certificate_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_certificate** | [**V0BuildCertificateUploadParams**](V0BuildCertificateUploadParams.md)| Build certificate parameters such as file name and its file size | 

### Return type

[**V0BuildCertificateResponseModel**](V0BuildCertificateResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **build_certificate_delete**
> V0BuildCertificateResponseModel build_certificate_delete(app_slug, build_certificate_slug)

Delete a build certificate

Delete an app's build certificate. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint to list all available build certificates of an app. Read more in our [Deleting an iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#deleting-an-ios-code-signing-file) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildCertificateApi.new

app_slug = 'app_slug_example' # String | App slug

build_certificate_slug = 'build_certificate_slug_example' # String | Build certificate slug


begin
  #Delete a build certificate
  result = api_instance.build_certificate_delete(app_slug, build_certificate_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildCertificateApi->build_certificate_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_certificate_slug** | **String**| Build certificate slug | 

### Return type

[**V0BuildCertificateResponseModel**](V0BuildCertificateResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_certificate_list**
> V0BuildCertificateListResponseModel build_certificate_list(app_slug, opts)

Get a list of the build certificates

List all the build certificates that have been uploaded to a specific app. Read more in our [Listing the uploaded iOS code signing files of an app](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#listing-the-uploaded-ios-code-signing-files-of-an-app) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildCertificateApi.new

app_slug = 'app_slug_example' # String | App slug

opts = { 
  _next: '_next_example', # String | Slug of the first build certificate in the response
  limit: 56 # Integer | Max number of build certificates per page is 50.
}

begin
  #Get a list of the build certificates
  result = api_instance.build_certificate_list(app_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildCertificateApi->build_certificate_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **_next** | **String**| Slug of the first build certificate in the response | [optional] 
 **limit** | **Integer**| Max number of build certificates per page is 50. | [optional] 

### Return type

[**V0BuildCertificateListResponseModel**](V0BuildCertificateListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_certificate_show**
> V0BuildCertificateResponseModel build_certificate_show(app_slug, build_certificate_slug)

Get a specific build certificate

Retrieve data of a specific build certificate. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint to list all available build certificates of an app. Read more in our [Getting a specific iOS code signing file's data](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#getting-a-specific-ios-code-signing-files-data) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildCertificateApi.new

app_slug = 'app_slug_example' # String | App slug

build_certificate_slug = 'build_certificate_slug_example' # String | Build certificate slug


begin
  #Get a specific build certificate
  result = api_instance.build_certificate_show(app_slug, build_certificate_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildCertificateApi->build_certificate_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_certificate_slug** | **String**| Build certificate slug | 

### Return type

[**V0BuildCertificateResponseModel**](V0BuildCertificateResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_certificate_update**
> V0BuildCertificateResponseModel build_certificate_update(app_slug, build_certificate_slug, build_certificate)

Update a build certificate

Update an uploaded build certificate's attributes. You can fetch the build certificate slug for this endpoint if you first call the [GET /apps/{app-slug}/build-certificates](https://api-docs.bitrise.io/#/build-certificate/build-certificate-list) endpoint. Read more in our [Updating an uploaded iOS code signing file](https://devcenter.bitrise.io/api/managing-ios-code-signing-files/#confirming-the-ios-code-signing-file-upload) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildCertificateApi.new

app_slug = 'app_slug_example' # String | App slug

build_certificate_slug = 'build_certificate_slug_example' # String | Build certificate slug

build_certificate = SwaggerClient::V0BuildCertificateUpdateParams.new # V0BuildCertificateUpdateParams | Build certificate parameters


begin
  #Update a build certificate
  result = api_instance.build_certificate_update(app_slug, build_certificate_slug, build_certificate)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildCertificateApi->build_certificate_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_certificate_slug** | **String**| Build certificate slug | 
 **build_certificate** | [**V0BuildCertificateUpdateParams**](V0BuildCertificateUpdateParams.md)| Build certificate parameters | 

### Return type

[**V0BuildCertificateResponseModel**](V0BuildCertificateResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



