# SwaggerClient::GenericProjectFileApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generic_project_file_confirm**](GenericProjectFileApi.md#generic_project_file_confirm) | **POST** /apps/{app-slug}/generic-project-files/{generic-project-file-slug}/uploaded | Confirm a generic project file upload
[**generic_project_file_delete**](GenericProjectFileApi.md#generic_project_file_delete) | **DELETE** /apps/{app-slug}/generic-project-files/{generic-project-file-slug} | Delete a generic project file
[**generic_project_file_list**](GenericProjectFileApi.md#generic_project_file_list) | **GET** /apps/{app-slug}/generic-project-files | Get a list of the generic project files
[**generic_project_file_show**](GenericProjectFileApi.md#generic_project_file_show) | **GET** /apps/{app-slug}/generic-project-files/{generic-project-file-slug} | Get a specific generic project file
[**generic_project_file_update**](GenericProjectFileApi.md#generic_project_file_update) | **PATCH** /apps/{app-slug}/generic-project-files/{generic-project-file-slug} | Update a generic project file
[**generic_project_files_create**](GenericProjectFileApi.md#generic_project_files_create) | **POST** /apps/{app-slug}/generic-project-files | Create a generic project file


# **generic_project_file_confirm**
> V0ProjectFileStorageResponseModel generic_project_file_confirm(app_slug, generic_project_file_slug)

Confirm a generic project file upload

This is the last step of uploading a generic project file to Bitrise. Confirm the generic project file upload and view the file on the Code Signing tab of a specific app. Read more in our [Confirming the upload](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#confirming-the-file-upload) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::GenericProjectFileApi.new

app_slug = 'app_slug_example' # String | App slug

generic_project_file_slug = 'generic_project_file_slug_example' # String | Generic project file slug


begin
  #Confirm a generic project file upload
  result = api_instance.generic_project_file_confirm(app_slug, generic_project_file_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling GenericProjectFileApi->generic_project_file_confirm: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **generic_project_file_slug** | **String**| Generic project file slug | 

### Return type

[**V0ProjectFileStorageResponseModel**](V0ProjectFileStorageResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **generic_project_file_delete**
> V0ProjectFileStorageResponseModel generic_project_file_delete(app_slug, generic_project_file_slug)

Delete a generic project file

Delete an app's generic project file. You can fetch an app's generic project file slug if you first list all the uploaded files with the [GET /apps/{app-slug}/generic-project-files](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-list) endpoint. Read more in our [Deleting a file](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#deleting-a-file) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::GenericProjectFileApi.new

app_slug = 'app_slug_example' # String | App slug

generic_project_file_slug = 'generic_project_file_slug_example' # String | Generic project file slug


begin
  #Delete a generic project file
  result = api_instance.generic_project_file_delete(app_slug, generic_project_file_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling GenericProjectFileApi->generic_project_file_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **generic_project_file_slug** | **String**| Generic project file slug | 

### Return type

[**V0ProjectFileStorageResponseModel**](V0ProjectFileStorageResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **generic_project_file_list**
> V0ProjectFileStorageListResponseModel generic_project_file_list(app_slug, opts)

Get a list of the generic project files

List all the generic project files that have been uploaded to a specific app. Read more in our [Listing the uploaded files of an app](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#listing-the-uploaded-files-of-an-app) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::GenericProjectFileApi.new

app_slug = 'app_slug_example' # String | App slug

opts = { 
  _next: '_next_example', # String | Slug of the first generic project file in the response
  limit: 56 # Integer | Max number of build certificates per page is 50.
}

begin
  #Get a list of the generic project files
  result = api_instance.generic_project_file_list(app_slug, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling GenericProjectFileApi->generic_project_file_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **_next** | **String**| Slug of the first generic project file in the response | [optional] 
 **limit** | **Integer**| Max number of build certificates per page is 50. | [optional] 

### Return type

[**V0ProjectFileStorageListResponseModel**](V0ProjectFileStorageListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **generic_project_file_show**
> V0ProjectFileStorageResponseModel generic_project_file_show(app_slug, generic_project_file_slug)

Get a specific generic project file

Retrieve data of a specific generic project file to check its attributes and optionally modify them with the [PATCH /apps/](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-update) endpoint. Read more in our [Retrieving a specific file's data](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#retrieving-a-specific-files-data) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::GenericProjectFileApi.new

app_slug = 'app_slug_example' # String | App slug

generic_project_file_slug = 'generic_project_file_slug_example' # String | Generic project file slug


begin
  #Get a specific generic project file
  result = api_instance.generic_project_file_show(app_slug, generic_project_file_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling GenericProjectFileApi->generic_project_file_show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **generic_project_file_slug** | **String**| Generic project file slug | 

### Return type

[**V0ProjectFileStorageResponseModel**](V0ProjectFileStorageResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **generic_project_file_update**
> V0ProjectFileStorageResponseModel generic_project_file_update(app_slug, generic_project_file_slug, generic_project_file)

Update a generic project file

Update a generic project file's attributes. You can fetch an app's generic project file slug if you first list all the uploaded files with the [GET /apps/{app-slug}/generic-project-files](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-list) endpoint. Read more in our [Updating an uploaded file](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#updating-an-uploaded-file) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::GenericProjectFileApi.new

app_slug = 'app_slug_example' # String | App slug

generic_project_file_slug = 'generic_project_file_slug_example' # String | Generic project file slug

generic_project_file = SwaggerClient::V0ProjectFileStorageDocumentUpdateParams.new # V0ProjectFileStorageDocumentUpdateParams | Generic project file parameters


begin
  #Update a generic project file
  result = api_instance.generic_project_file_update(app_slug, generic_project_file_slug, generic_project_file)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling GenericProjectFileApi->generic_project_file_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **generic_project_file_slug** | **String**| Generic project file slug | 
 **generic_project_file** | [**V0ProjectFileStorageDocumentUpdateParams**](V0ProjectFileStorageDocumentUpdateParams.md)| Generic project file parameters | 

### Return type

[**V0ProjectFileStorageResponseModel**](V0ProjectFileStorageResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **generic_project_files_create**
> V0ProjectFileStorageResponseModel generic_project_files_create(app_slug, generic_project_file)

Create a generic project file

Create a temporary pre-signed upload URL (expires in 10 minutes) for the generic project file and upload it to AWS with a simple `curl` request. To complete the uploading process and view your files on the Code Signing tab of your app, continue with the [POST /apps/{app-slug}/generic-project-files/{generic-project-file-slug}/uploaded](https://api-docs.bitrise.io/#/generic-project-file/generic-project-file-confirm) endpoint. Read more in our [Creating and uploading files to Generic File Storage](https://devcenter.bitrise.io/api/managing-files-in-generic-file-storage/#creating-and-uploading-files-to-generic-file-storage) guide.

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::GenericProjectFileApi.new

app_slug = 'app_slug_example' # String | App slug

generic_project_file = SwaggerClient::V0ProjectFileStorageUploadParams.new # V0ProjectFileStorageUploadParams | Generic project file parameters


begin
  #Create a generic project file
  result = api_instance.generic_project_files_create(app_slug, generic_project_file)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling GenericProjectFileApi->generic_project_files_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **generic_project_file** | [**V0ProjectFileStorageUploadParams**](V0ProjectFileStorageUploadParams.md)| Generic project file parameters | 

### Return type

[**V0ProjectFileStorageResponseModel**](V0ProjectFileStorageResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



