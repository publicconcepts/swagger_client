# SwaggerClient::V0AddonsListResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Array&lt;AddonsAddon&gt;**](AddonsAddon.md) |  | [optional] 


