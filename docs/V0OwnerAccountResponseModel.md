# SwaggerClient::V0OwnerAccountResponseModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_type** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 


