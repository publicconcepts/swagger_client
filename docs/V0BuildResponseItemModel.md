# SwaggerClient::V0BuildResponseItemModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**abort_reason** | **String** |  | [optional] 
**branch** | **String** |  | [optional] 
**build_number** | **Integer** |  | [optional] 
**commit_hash** | **String** |  | [optional] 
**commit_message** | **String** |  | [optional] 
**commit_view_url** | **String** |  | [optional] 
**environment_prepare_finished_at** | **String** |  | [optional] 
**finished_at** | **String** |  | [optional] 
**is_on_hold** | **BOOLEAN** |  | [optional] 
**original_build_params** | **String** |  | [optional] 
**pull_request_id** | **Integer** |  | [optional] 
**pull_request_target_branch** | **String** |  | [optional] 
**pull_request_view_url** | **String** |  | [optional] 
**slug** | **String** |  | [optional] 
**stack_config_type** | **String** |  | [optional] 
**stack_identifier** | **String** |  | [optional] 
**started_on_worker_at** | **String** |  | [optional] 
**status** | **Integer** |  | [optional] 
**status_text** | **String** |  | [optional] 
**tag** | **String** |  | [optional] 
**triggered_at** | **String** |  | [optional] 
**triggered_by** | **String** |  | [optional] 
**triggered_workflow** | **String** |  | [optional] 


