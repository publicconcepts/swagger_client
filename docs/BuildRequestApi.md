# SwaggerClient::BuildRequestApi

All URIs are relative to *https://api.bitrise.io/v0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**build_request_list**](BuildRequestApi.md#build_request_list) | **GET** /apps/{app-slug}/build-requests | List the open build requests for an app
[**build_request_update**](BuildRequestApi.md#build_request_update) | **PATCH** /apps/{app-slug}/build-requests/{build-request-slug} | Update a build request


# **build_request_list**
> V0BuildRequestListResponseModel build_request_list(app_slug)

List the open build requests for an app

List the existing open build requests of a specified Bitrise app

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: AddonAuthToken
  config.api_key['Bitrise-Addon-Auth-Token'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Bitrise-Addon-Auth-Token'] = 'Bearer'

  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildRequestApi.new

app_slug = 'app_slug_example' # String | App slug


begin
  #List the open build requests for an app
  result = api_instance.build_request_list(app_slug)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildRequestApi->build_request_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 

### Return type

[**V0BuildRequestListResponseModel**](V0BuildRequestListResponseModel.md)

### Authorization

[AddonAuthToken](../README.md#AddonAuthToken), [PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **build_request_update**
> V0BuildRequestUpdateResponseModel build_request_update(app_slug, build_request_slug, build_request)

Update a build request

Update a specific build request of a specific app

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: PersonalAccessToken
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::BuildRequestApi.new

app_slug = 'app_slug_example' # String | App slug

build_request_slug = 'build_request_slug_example' # String | Build request slug

build_request = SwaggerClient::V0BuildRequestUpdateParams.new # V0BuildRequestUpdateParams | Build request parameters


begin
  #Update a build request
  result = api_instance.build_request_update(app_slug, build_request_slug, build_request)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling BuildRequestApi->build_request_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app_slug** | **String**| App slug | 
 **build_request_slug** | **String**| Build request slug | 
 **build_request** | [**V0BuildRequestUpdateParams**](V0BuildRequestUpdateParams.md)| Build request parameters | 

### Return type

[**V0BuildRequestUpdateResponseModel**](V0BuildRequestUpdateResponseModel.md)

### Authorization

[PersonalAccessToken](../README.md#PersonalAccessToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



